Installation instructions for Piperka crawler
=============================================

I'm using Debian for all my systems.  YMMV if you use some other
environment.  There's a number of dependencies required, I've listed
them along with the parts that require them instead of placing them
all on one line.

```
sudo apt install ghc cabal-install zlib1g-dev libpq-dev libicu-dev
```

```
cabal sandbox init
cabal configure --enable-tests
cabal install --only-dependencies --enable-tests
cabal build
```

Perl dependencies
-----------------

The parser is written in perl and it needs its own dependencies
installed.

```
sudo apt install liblinux-prctl-perl libjson-perl libxml-feed-perl
```

Database
--------

The crawler uses a PostgreSQL database.  To set it up for the test
suite, create a local database named `crawler-test` and make sure that
the user you're running the tests as has access to it.

```
sudo apt install postgresql
sudo -u postgres createuser $(whoami)
sudo -u postgres createddb crawler-test -O $(whoami)
```

Using and testing the crawler
-----------------------------

The crawler still needs a couple of more dependencies:

```
sudo apt install curl memcached
```

Using the repl is often useful when running the test suite.  Start it
with `cabal repl crawler-test`.  If `ghci` gets confused and offers
some other module that `Crawler.Test`, use `:m` to select it.  A newer
GHC doesn't seem to like my cabal file for some reason.

Piperka's server is running Debian Buster and builds its binaries
locally.  Specifically that means that it's using GHC 8.4 for the
production builds.
