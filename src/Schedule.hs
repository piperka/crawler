{-# LANGUAGE OverloadedStrings #-}

module Schedule where

import Control.Error.Util
import Control.Monad.Trans.Except
import Control.Parallel.Strategies
import qualified Data.IntMap as IntMap
import System.IO
import Options.Applicative
import Text.Printf

import Crawler.DB
import Schedule.DB
import Schedule.Scoring

main :: IO ()
main = do
  settings <- execParser opts
  let weeks = 12
      months = 5
  (wStart, mStart, sched) <- either error id <$> getSchedules settings weeks months
  either (error . show) (const $ return ()) =<<
    (runExceptT $ withConnTransactionally settings $
     saveUpdateSchedule $
     parMap rdeepseq
      (\(c, (ini, xs)) -> (c
                          , maybe [] id . hush . findPeaks . scoreWeek $
                            weeklySchedule wStart weeks xs
                          , weeklyUpdateCount wStart ini weeks xs
                          , monthlyUpdateCount mStart ini months xs)) $
     IntMap.toList sched)
  where
    dbArg = DBConfig
      <$> argument str (metavar "DB" <> value "postgresql://kaol@/piperka")
      <*> argument str (metavar "SCHEMA" <> value "piperka")
    opts = info (dbArg <**> helper) $
      fullDesc <> progDesc "Piperka Schedule infer"

-- for debug
saveCsv :: FilePath -> [(Int, [Double])] -> IO ()
saveCsv file xs = withFile file WriteMode $ \h -> do
  mapM_ (\(cid, ss) -> do
            hPutStr h (show cid)
            mapM_ (\s -> hPutStr h ('\t':(printf "%.5f" s))) ss
            hPutStrLn h ""
            print cid
        ) xs

-- for debug
saveCsv2 :: FilePath -> [(Int, [Double])] -> IO ()
saveCsv2 file xs = withFile file WriteMode $ \h -> do
  hPutStr h "-\t"
  hPutStrLn h . foldr1 (\a b -> a ++ "\t" ++ b) $ map (show . fst) xs
  let out = parMap rpar (\i -> (i, foldr1 (\a b -> a ++ "\t" ++ b) $
                                   map (printf "%.5f" . (!! i) . snd) xs)) [0..167]
  mapM_ (\(i, x) -> do
            hPutStr h $ (show i) ++ "\t"
            hPutStrLn h x) out
