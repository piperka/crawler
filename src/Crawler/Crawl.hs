{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Crawler.Crawl (crawl, crawl') where

import Control.Concurrent
import Control.Exception
import Control.Lens
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.State
import Data.ByteString.Lazy (ByteString)
import Data.Default.Class
import qualified Data.IntMap as IntMap
import Data.Maybe
import qualified Data.Text.ICU as ICU
import qualified Data.Vector as V

import Crawler.Curl.Download
import Crawler.Parser.Perl.Comm
import Crawler.Parser.Perl.Actions
import Crawler.Types

data CrawlProcesses = CrawlProcesses
  { downloadThreads :: [ThreadId]
  , submits :: Chan (Int, CrawlTarget)
  , responses :: MVar (Int, Either CrawlError ByteString)
  }

-- | Non-interactive silent crawl with optional cutoff
crawl :: ArchiveState -> Maybe Int -> IO CrawlState
crawl archive cutoff = do
  let control cid ord ev = case cutoff of
        Nothing -> return ()
        Just n -> if ord - archive IntMap.! cid ^. pageCount >= n
                     && (not $ isTerminal ev)
                  then modify $ IntMap.adjust
                       (endCondition .~ (Just EndCutoff)) cid
                  else return ()
      isTerminal (TerminalEvent _ _ _) = True
      isTerminal _ = False
  bracket (startParser def) closeParser $ \pHandle ->
    crawl' def archive pHandle control >>= takeMVar

-- | Crawl with control for action after each crawl event
crawl'
  :: ProgramConfig
  -> ArchiveState
  -> ParserHandle
  -> (Int -> Int -> CrawlEvent -> StateT CrawlState IO ())
  -> IO (MVar CrawlState)
crawl' config archive pHandle control = if null archive then newMVar mempty else do
  resultVar <- newEmptyMVar
  void $ forkIO $ crawlRunner config archive pHandle control >>=
    putMVar resultVar
  return resultVar

crawlRunner
  :: ProgramConfig
  -> ArchiveState
  -> ParserHandle
  -> (Int -> Int -> CrawlEvent -> StateT CrawlState IO ())
  -> IO CrawlState
crawlRunner config archive pHandle control =
  bracket crawlInit crawlFinish $ \processes -> do
  let subm = submits processes
      responseMVar = responses processes
      world = IntMap.map (\x -> def & originalArchive .~ x) archive
      initialTargets =
        map (\(i, (ArchiveEntry _ pages _ cfg)) ->
               let hd = V.head pages in
                 (i, initialCrawl cfg hd)) $
        IntMap.assocs archive
  mapM_ (writeChan subm) initialTargets
  parserInst <- IntMap.fromList <$>
    (mapM (\(i, e) -> (i,) <$> startInstance pHandle (e ^. cConfig)) $
     IntMap.assocs archive)
  endState <- flip execStateT world $
    let waitAndProcess n = do
          (i, resp) <- liftIO $ takeMVar responseMVar
          start <- gets (IntMap.! i)
          let entry = archive IntMap.! i
              name = maybe (V.head $ entry ^. archivePages) id $
                     listToMaybe $ start ^. newPages
              cfg = view cConfig entry
              ord = V.length (entry ^. archivePages) +
                    length (start ^. newPages)
              hc = view headCropCount entry
          pars <- runExceptT $ do
            resp' <- withExceptT EndCrawl $ ExceptT $ return resp
            maybe (return ()) throwE $ start ^. endCondition
            withExceptT EndParser . ExceptT . liftIO $
              feedPage (parserInst IntMap.! i) name resp'
          tgt <- runExceptT $ do
            parsed <- ExceptT . return $ pars
            tgt <- maybe (throwE EndNoNext) return =<<
              (ExceptT $ parseAction archive (i, parsed))
            when (not $ isJust (ICU.find httpRegex $ tgt ^. nextURL)
                  || allowFile cfg) $ throwE $ EndCrawl FileForbidden
            return tgt
          either
            (\e -> modify $ IntMap.adjust (set endCondition (Just e)) i)
            (const $ return ()) tgt
          let event = case (tgt, either (const Nothing) Just $ pars) of
                (Left e, p) -> TerminalEvent e hc p
                (Right t, Just p) -> ParseEvent p t
                (Right t, _) -> TargetEvent t
          control i ord event
          end <- gets ((^. endCondition) . (IntMap.! i))
          n' <- case (isJust end, tgt) of
            (True, _) -> return $ n-1
            (_, Left _) -> return $ n-1
            (_, Right tgt') -> liftIO $ writeChan subm (i, tgt') >> return n
          if n' > 0 then waitAndProcess n' else return ()
    in mapM_ (\(i,tgt) -> let ord = archive IntMap.! i ^. pageCount
                          in control i ord (TargetEvent tgt)) initialTargets >>
       (waitAndProcess $ IntMap.size archive)
  mapM_ closeInstance parserInst
  return endState
  where
    crawlInit = do
      subm <- newChan
      responseMVar <- newEmptyMVar
      let thr = config ^. crawlThreads
      CrawlProcesses
        -- Start Download threads
        <$> (replicateM thr $ forkIO $ forever $ do
                (i, tgt) <- readChan subm
                let cc = (archive IntMap.! i) ^. cConfig
                resp <- downloadPage cc tgt
                putMVar responseMVar (i, resp))
        <*> pure subm
        <*> pure responseMVar
    crawlFinish cfg = do
      mapM_ killThread $ downloadThreads cfg
    staticRegex = ICU.regex [ICU.Multiline] "^#\\s+(?:rss|url):\\s*(\\S+)"
    httpRegex = ICU.regex [] "^(?:http|https)://"
    initialCrawl cfg hd =
      maybe (def
             & noContent .~ firstNoContent cfg
             & nextURL .~ urlBase cfg <> hd <> urlTail cfg) id $
      ((ICU.find staticRegex (startHook cfg)) >>= ICU.group 1 >>=
        Just . flip (set nextURL) def .
        (maybe id const $ extraURL cfg))
