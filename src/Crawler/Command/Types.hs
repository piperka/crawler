{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}

module Crawler.Command.Types where

import Control.Lens (makeLenses)
import Data.Aeson
import Data.Int
import Data.IntSet (IntSet)
import qualified Data.HashMap.Strict as Map
import Data.Scientific (toBoundedInteger)
import Data.Text (Text)
import Data.Text.Read (decimal)
import Data.Time.Clock
import Text.Show.Functions ()

import Crawler.Types

data Crawl = Crawl
  { _startTime :: UTCTime
  , _crawlHalt :: Bool
  , _crawlProgress :: [(Int, Int, CrawlEvent)]
  , _initialArchive :: ArchiveEntry
  , _crawlResult :: Maybe SiteState
  , _parserId :: Int16
  , _deletions :: IntSet
  } deriving (Show)

makeLenses ''Crawl

data CrawlHandle = CrawlHandle ((Crawl -> Crawl) -> IO ()) (IO Crawl)

data CrawlComm = CrawlComm
  { sendData :: CrawlResult -> IO ()
  , receiveData :: IO CrawlControl
  , getCrawl :: IO Crawl
  , adjust :: (Crawl -> Crawl) -> IO ()
  }

data CrawlControl
  = CmdStart
  { parserType :: Int16
  , crawlerConfigParams :: (Text -> Text -> Maybe Text -> Maybe Text -> CrawlerConfig)
                        -> CrawlerConfig
  }
  | CmdStop
  | CmdReset
  | CmdAddPages [Text]
  | CmdSave Int32 [(Int, Int)] Text Text
  | CmdDiscover Text Text Text Text
  | CmdRemovePage Int
  | CmdCrop Int
  | CmdGoComics Text
  | CmdUnknown
  deriving (Show)

instance FromJSON CrawlControl where
  parseJSON = withObject "CrawlControl" $ \v -> do
    cmd :: Text <- v .: "cmd"
    case cmd of
      "start" -> CmdStart
        <$> v .: "parser_type"
        <*> (do
                (a,b,c,d) <- (,,,)
                  <$> v .: "url_base"
                  <*> v .: "url_tail"
                  <*> v .:? "extra_url"
                  <*> v .:? "extra_data"
                return $ \f -> f a b c d
            )
      "halt" -> return CmdStop
      "reset" -> return CmdReset
      "save" ->
        (CmdSave
         <$> v .: "cid"
         <*> (do
                 (Object o) <- v .: "bookmarkMoves"
                 maybe (fail "no parse") return $ sequence $ map
                   (\(a,b) -> (,)
                     <$> (either (const Nothing) Just $ fst <$> decimal a)
                     <*> (case b of
                            Number n -> toBoundedInteger n
                            _ -> Nothing)
                   ) $ Map.toList o
             )
         <*> v .: "homepage"
         <*> v .: "fixedHead"
        )
      "discover" ->
        (CmdDiscover
         <$> v .: "url_base"
         <*> v .: "url_tail"
         <*> v .: "source"
         <*> v .: "target")
      "add" -> CmdAddPages
        <$> v .: "pages"
      "remove" -> CmdRemovePage
        <$> v .: "ord"
      "crop" -> CmdCrop
        <$> v .: "ord"
      "gocomics" -> CmdGoComics
        <$> v .: "url"
      _ -> fail "unknown command"

instance ToJSON CrawlControl where
  toJSON CmdStop = object ["cmd" .= ("halt" :: Text)]
  toJSON _ = Null

data CrawlResult
  = CrawlProgress Int CrawlEvent
  | CrawlAddPage Int (Maybe Text)
  | CrawlRemovePage Int
  | CrawlMarkPageRemoval Int
  | CrawlCrop Int
  | CrawlReset
  | CrawlErr Text
  | CrawlAck Text
  | CrawlDone SiteState
  | CrawlClose
  | CrawlDiscover [Int]
  | CrawlGoComicsParams Text
  deriving (Eq)

instance ToJSON CrawlResult where
  toJSON (CrawlProgress ord ev) = object ["ord" .= ord, "ev" .= ev]
  toJSON (CrawlAddPage ord (Just page)) = object ["ord" .= ord, "add" .= page]
  toJSON (CrawlAddPage ord Nothing) = object ["ord" .= ord, "fixed_head" .= True]
  toJSON (CrawlRemovePage ord) = object ["remove" .= ord]
  toJSON (CrawlMarkPageRemoval ord) = object ["mark_removal" .= ord]
  toJSON (CrawlCrop ord) = object ["crop" .= ord]
  toJSON CrawlReset = object ["reset" .= True]
  toJSON (CrawlErr err) = object ["err" .= err]
  toJSON (CrawlAck msg) = object ["msg" .= msg]
  toJSON (CrawlDone end) = object ["ev" .= object ["done" .= toJSON end]]
  toJSON CrawlClose = object ["close" .= True]
  toJSON (CrawlDiscover match) = object ["discover" .= toJSON match]
  toJSON (CrawlGoComicsParams home) =
    object ["ev" .= object ["gocomics" .= True], "homepage" .= home]
