{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}

module Crawler.Parser.Perl.Comm
  ( ParserHandle
  , ParserInstance
  , startParser
  , startParser'
  , startInstance
  , feedPage
  , closeInstance
  , closeParser
  ) where

-- Communication with the parser process

import Control.Concurrent
import Control.Exception
import Control.Lens
import Data.Aeson (eitherDecode, encode)
import Data.Binary.Get (runGet, getInt32host)
import Data.Binary.Put (runPut, putWord8, putInt32host, putLazyByteString)
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as B
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8)
import Data.Int (Int32)
import Data.IORef
import Data.Word8
import System.IO
import System.Process

import Crawler.Types

hPut' :: Handle -> ByteString -> IO ()
hPut' = B.hPut
--hPut' h x = B.appendFile "comm.debug" ("> " <> x <> "\n") >> B.hPut h x

hGet' :: Handle -> Int -> IO ByteString
hGet' = B.hGet
{-
hGet' h i = do
  x <- B.hGet h i
  B.appendFile "comm.debug" ("< " <> x <> "\n")
  return x
-}

data ParserHandle = ParserHandle
  { parserIn :: Handle
  , parserOut :: Handle
  , process :: ProcessHandle
  , uniqueAccess :: forall a. IO a -> IO a
  , instIdRef :: IORef Int32
  }

data ParserInstance = ParserInstance
  { parser :: ParserHandle
  , instId :: Int32
  }

-- TODO: Store std_err output as part of the crawl result
startParser :: ProgramConfig -> IO ParserHandle
startParser cfg = startParser' cfg Inherit

startParser' :: ProgramConfig -> StdStream -> IO ParserHandle
startParser' cfg stderrStream = do
  let dir = cfg ^. parserDir
  (Just hIn, Just hOut, _, pHandle) <-
    createProcess (System.Process.proc "bin/perl_parser"
                   []){ std_in = CreatePipe
                      , std_out = CreatePipe
                      , std_err = stderrStream
                      , cwd = Just dir
                      }
  r <- newIORef 0
  uniq <- newEmptyMVar
  return $ ParserHandle hIn hOut pHandle (bracket_ (putMVar uniq ()) (takeMVar uniq)) r

startInstance :: ParserHandle -> CrawlerConfig -> IO ParserInstance
startInstance h cfg = uniqueAccess h $ do
  let hIn = parserIn h
      ini = encode cfg
  token <- atomicModifyIORef' (instIdRef h) $ \n -> let n' = n+1 in (n', n')
  hPut' hIn $ runPut $ do
    putWord8 _i
    putInt32host token
    putInt32host $ fromIntegral $ B.length ini
    putLazyByteString ini
  hFlush hIn
  return $ ParserInstance h token

-- Give the raw HTTP response to parser
feedPage :: ParserInstance -> Text -> ByteString -> IO (Either String ParseResult)
feedPage cfg n d = uniqueAccess (parser cfg) $ do
  let hIn = parserIn $ parser cfg
      hOut = parserOut $ parser cfg
      n' = B.fromStrict $ encodeUtf8 n
  hPut' hIn $ runPut $ do
    putWord8 _p
    putInt32host $ instId cfg
    -- Send size
    putInt32host $ fromIntegral $ B.length n'
    -- Send page name
    putLazyByteString n'
    -- Send size
    putInt32host $ fromIntegral $ B.length d
    -- Send data
    putLazyByteString d
  hFlush hIn
  -- Read size
  len <- fromIntegral . runGet getInt32host <$> hGet' hOut 4
  -- Read reply JSON
  eitherDecode <$> hGet' hOut len

closeInstance :: ParserInstance -> IO ()
closeInstance h = uniqueAccess (parser h) $ do
  let hIn = parserIn $ parser h
  hPut' hIn $ runPut $ do
    putWord8 _c
    putInt32host (instId h)
  hFlush hIn

closeParser :: ParserHandle -> IO ()
closeParser cfg = do
  hClose $ parserIn cfg
  _ <- waitForProcess (process cfg)
  return ()
