{-# LANGUAGE OverloadedStrings #-}

module Crawler.DB.Score where

import Data.Int
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.Time.Clock
import Data.Time.LocalTime
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session hiding (sql)
import Hasql.Statement

increaseScores :: Session ()
increaseScores = do
  statement () $ Statement
    "UPDATE crawler_config SET \
    \update_score = update_score+update_value+3"
    E.unit D.unit True
  statement () $ Statement
    "UPDATE crawler_config SET \
    \update_value = update_value*0.991 \
    \WHERE update_value > 0.00000000000001"
    E.unit D.unit True

decreaseTriggered :: Session ()
decreaseTriggered = statement () $
  Statement "UPDATE crawler_config SET update_score = update_score - 60 \
            \WHERE update_score > 60" E.unit D.unit True

decreaseFound :: [Int32] -> Session ()
decreaseFound xs = do
  statement xs $ Statement sql encoder D.unit True
  statement xs $ Statement sql2 encoder D.unit True
  where
    encoder = E.param $ E.array $ E.dimension foldl $ E.element E.int4
    sql = "UPDATE crawler_config \
          \SET update_score=update_score-19*(update_value+12), \
          \update_value=update_value+12, \
          \last_updated = now() WHERE cid = any($1 :: int[])"
    sql2 = "UPDATE hiatus_status SET active=false \
           \WHERE cid = any($1 :: int[]) AND active"

-- Used for testing
getScores :: [Int32] -> Session (IntMap (UTCTime, Float, Float))
getScores = flip statement $
            Statement sql encoder (IntMap.fromAscList <$> D.rowList decoder) True
  where
    encoder = E.param $ E.array $ E.dimension foldl $ E.element E.int4
    decoder = (,)
      <$> (fromIntegral <$> D.column D.int4)
      <*> ((,,)
           <$> (localTimeToUTC utc <$> D.column D.timestamp)
           <*> D.column D.float4
           <*> D.column D.float4
          )
    sql = "SELECT cid, last_updated, update_score, update_value \
          \FROM crawler_config WHERE cid = any($1 :: int[]) ORDER BY cid"
