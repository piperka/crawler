{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Crawler.DB.Store where

import Contravariant.Extras.Contrazip
import Control.Lens
import Data.Bool
import Data.Functor.Contravariant
import Data.Int
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.Text (Text)
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session hiding (sql)
import qualified Hasql.Session as S
import Hasql.Statement

import Crawler.DB.Score
import Crawler.Types

updateCounts :: ArchiveState -> CrawlState -> IntMap Int
updateCounts = (IntMap.filter ((>0)) .) . IntMap.intersectionWith
  (\a c -> (V.length (a ^. archivePages) + length (c ^. newPages) - 1 +
            if c ^. addFixedHead then 1 else 0) - a ^. pageCount -
           if c ^. replacingHead then 1 else 0
  )

saveCrawlScores :: [Int32] -> Session ()
saveCrawlScores updated = do
  increaseScores
  decreaseTriggered
  decreaseFound updated

saveCrawl :: CrawlState -> Session ()
saveCrawl crawl = do
  let inp f = map (\(a,b,c) -> (fromIntegral a, fromIntegral b, c)) $
        concatMap
        (uncurry (zipWith (\a (b,c) -> (a,b,c)) . repeat)) $
        IntMap.assocs $ IntMap.mapWithKey f crawl
      start i = let s = crawl IntMap.! i in
        V.length (view (originalArchive . archivePages) s) -
        bool 0 1 (view replacingHead s)
      pages :: [(Int32, Int32, Maybe Text)] = inp $
        \i ss -> zip [start i..] $ reverse $
                 (if ss ^. addFixedHead then (Nothing:) else id) $
                 map Just $ ss ^. newPages
      frags :: [(Int32, Int32, Vector Text)] = inp $
        \i -> zip [start i - 1..] . reverse . (^. fragments)
      cids = map fromIntegral $ IntMap.keys crawl
  prepareMassUpdate
  statement pages savePages
  statement (map (\(a,b,_) -> (a,b)) frags) deleteOldFragments
  mapM_ (flip statement saveFragment) $
    concatMap (\(a,b,c) -> zipWith (\i x -> (a,b,i,x)) [1..] $ V.toList c) frags
  mapM_ (flip statement updateMax) cids

savePages :: Statement [(Int32, Int32, Maybe Text)] ()
savePages = Statement sql encoder D.unit True
  where
    encArray = E.param . E.array . E.dimension foldl
    enc = encArray . E.element
    enc' = encArray . E.nullableElement
    encoder = unzip3 >$< contrazip3 (enc E.int4) (enc E.int4) (enc' E.text)
    sql = "INSERT INTO updates (cid, ord, name) \
          \SELECT * FROM UNNEST($1, $2, $3) \
          \ON CONFLICT (cid, ord) DO UPDATE SET name=EXCLUDED.name"

deleteOldFragments :: Statement [(Int32, Int32)] ()
deleteOldFragments = Statement sql encoder D.unit True
  where
    list = E.param . E.array . E.dimension foldl . E.element
    encoder = unzip >$< contrazip2 (list E.int4) (list E.int4)
    sql = "DELETE FROM page_fragments \
          \WHERE (cid, ord) IN (SELECT * FROM unnest($1, $2))"

saveFragment :: Statement (Int32, Int32, Int32, Text) ()
saveFragment = Statement sql encoder D.unit True
  where
    encoder = contrazip4 (E.param E.int4) (E.param E.int4)
              (E.param E.int4) (E.param E.text)
    sql = "INSERT INTO page_fragments (cid, ord, subord, fragment) \
          \VALUES ($1, $2, $3, $4)"

prepareMassUpdate :: Session ()
prepareMassUpdate = do
  S.sql "CREATE TEMPORARY TABLE IF NOT EXISTS piperka_temp_mass_update (lock bool not null)"
  S.sql "INSERT INTO piperka_temp_mass_update VALUES (true)"

updateMax :: Statement Int32 ()
updateMax = Statement sql (E.param E.int4) D.unit True
  where
    sql = "INSERT INTO max_update_ord (cid, ord, subord) \
          \SELECT $1, ord, coalesce(max(subord), 1) FROM \
          \(SELECT cid, max(ord) AS ord FROM updates WHERE updates.cid=$1 \
          \ GROUP BY cid) AS max_upd \
          \LEFT JOIN page_fragments USING (cid, ord) GROUP BY ord \
          \ON CONFLICT (cid) DO UPDATE SET ord=EXCLUDED.ord, subord=EXCLUDED.subord"

-- TODO: Adding only fragments should update last updated as well
updateLastUpdated :: Statement [Int32] ()
updateLastUpdated = Statement sql encoder D.unit True
  where
    encoder = E.param $ E.array $ E.dimension foldl $ E.element E.int4
    sql = "UPDATE crawler_config SET last_updated=now() \
          \WHERE cid = ANY($1 :: int[])"
