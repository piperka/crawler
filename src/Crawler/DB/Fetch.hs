{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

-- Get the config needed by the parser from the database

module Crawler.DB.Fetch where

import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Data.ByteString (ByteString)
import Data.Int
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.Maybe
import Data.Text (Text)
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session hiding (sql)
import qualified Hasql.Session as S
import Hasql.Statement

import Crawler.DB.Score
import Crawler.Types

configDecoder :: D.Row CrawlerConfig
configDecoder = makeCrawlerConfig
  <$> D.nullableColumn D.text
  <*> D.column D.text
  <*> D.column D.text
  <*> D.column D.text
  <*> D.nullableColumn D.text
  <*> D.nullableColumn D.text

configSelect
  :: ByteString
configSelect = 
  "SELECT cid, head_crop_count, text_hook, start_hook, \
  \url_base, url_tail, extra_url, extra_data \
  \FROM comics JOIN crawler_config USING (cid) \
  \JOIN parsers ON parser_type=parsers.id \
  \WHERE "

hourlyQuery :: Statement () (IntMap (Int, CrawlerConfig))
hourlyQuery = Statement sql E.unit
              (IntMap.fromAscList <$> D.rowList decoder) True
  where
    decoder = (,)
      <$> (fromIntegral <$> D.column D.int4)
      <*> ((,)
           <$> (fromIntegral <$> D.column D.int2)
           <*> configDecoder)
    sql = configSelect <>
      " update_score > 60 \
      \OR cid IN (\
      \ SELECT cid FROM ( \
      \  SELECT cid, how, \
      \  array_position(schedule, 0::smallint) IS NOT NULL AS first_hour,\
      \  array_position(schedule, 167::smallint) IS NOT NULL AS last_hour, \
      \  min(abs(n-how.how::smallint)) AS proxmity \
      \  FROM inferred_schedule, unnest(schedule) AS n, \
      \  (SELECT (extract(isodow FROM current_timestamp)-1)*24 + \
      \   extract(hour from current_timestamp) AS how) AS how \
      \  GROUP BY cid, how, schedule) AS x\
      \ JOIN crawler_config USING (cid) \
      \ WHERE last_updated < current_timestamp - '22 hours' :: interval AND \
      \ (proxmity <= 1 OR how=167 AND first_hour OR how=0 AND last_hour) \
      \) ORDER BY cid"

singleQuery :: Statement Int32 (Maybe (Int, CrawlerConfig))
singleQuery = Statement sql (E.param E.int4)
              (D.rowMaybe (D.column D.int4 *>
                           ((,)
                            <$> (fromIntegral <$> D.column D.int2)
                            <*> configDecoder))) True
  where
    sql = configSelect <> " cid=$1"

fetchHourly :: Session ArchiveState
fetchHourly = do
  S.sql "begin"
  increaseScores
  upd <- statement () hourlyQuery
  pg <- statement (map fromIntegral $ IntMap.keys upd) fetchPagesQuery
  S.sql "rollback"
  return $ IntMap.intersectionWith (uncurry . uncurry ArchiveEntry) pg upd

fetchSingle ::Int32 -> Session (Maybe (Int, CrawlerConfig))
fetchSingle = flip statement singleQuery

fetchPages :: [Int32]
           -> Session (IntMap (Int, Vector Text))
fetchPages = flip statement fetchPagesQuery

fetchSingleArchive :: Bool -> Int32 -> Session (Maybe ArchiveState)
fetchSingleArchive useCrop cid =
  runMaybeT $ (MaybeT $ statement cid singleQuery) >>= \upd -> lift $
  IntMap.map (flip uncurry upd . uncurry ArchiveEntry) <$>
  case useCrop of
    True -> statement [cid] fetchPagesQuery
    False -> do
      let f v = let l = V.length v - if isNothing $ V.last v then 1 else 0
                    v' = fromJust <$> V.take l v
                in (l, v')
      fmap f <$> fetchPages' [cid]

fetchMultiArchive :: [Int32] -> Session (Maybe ArchiveState)
fetchMultiArchive cids = runMaybeT $ do
  cfgs <- IntMap.fromList <$>
    mapM (\cid -> fmap (fromIntegral cid,) $ MaybeT $ statement cid singleQuery) cids
  lift (IntMap.intersectionWith (flip (uncurry . uncurry ArchiveEntry)) cfgs <$>
        statement cids fetchPagesQuery)

-- TODO: Fetch fragments too.
fetchPagesQuery :: Statement [Int32] (IntMap (Int, Vector Text))
fetchPagesQuery =
  Statement sql encoder (IntMap.fromAscList <$> D.rowList decoder) True
  where
    decoder = (,)
      <$> (fromIntegral <$> D.column D.int4)
      <*> ((,)
           <$> (fromIntegral <$> D.column D.int4)
           <*> D.column (D.array $ D.dimension V.replicateM $ D.element D.text))
    encoder = E.param $ E.array (E.dimension foldl $ E.element E.int4)
    sql = "SELECT cid, \
          \(SELECT max(ord) AS top FROM updates AS u2 WHERE u2.cid=x.cid) AS m, \
          \ARRAY_AGG(page) \
          \FROM (SELECT cid, MAX(name) AS page FROM updates \
          \ JOIN (SELECT cid, max(ord)-head_crop_count AS mx \
          \  FROM updates JOIN crawler_config USING (cid) \
          \  WHERE name IS NOT NULL GROUP BY cid, head_crop_count) AS mx USING (cid) \
          \ WHERE cid = ANY ($1 :: int[]) AND ord <= GREATEST(0, mx) \
          \ GROUP BY cid, ord ORDER BY cid, ord DESC) AS x \
          \GROUP BY x.cid ORDER BY cid"

-- Fetch pages without applying the head offset
fetchPages' :: [Int32] -> Session (IntMap (Vector (Maybe Text)))
fetchPages' xs = statement xs stmt
  where
    stmt = Statement sql encoder
           (IntMap.fromList . (map ((,V.empty) . fromIntegral) xs <>)
            <$> D.rowList decoder) True
    decoder = (,)
      <$> (fromIntegral <$> D.column D.int4)
      <*> D.column (D.array $ D.dimension V.replicateM $ D.nullableElement D.text)
    encoder = E.param $ E.array (E.dimension foldl $ E.element E.int4)
    sql = "SELECT cid, ARRAY_AGG(page) \
          \FROM (SELECT cid, MAX(name) AS page FROM updates \
          \ WHERE cid= ANY ($1 :: int[]) \
          \ GROUP BY cid, ord ORDER BY cid, ord DESC) AS x \
          \GROUP BY x.cid ORDER BY cid"

fetchNewCid :: Session Int32
fetchNewCid = statement () stmt
  where
    stmt = Statement sql E.unit (D.singleRow $ D.column D.int4) True
    sql = "SELECT COALESCE(MAX(cid)+1,1) FROM comics"

fetchParser :: Int16
            -> Session (Maybe (Text -> Text -> Maybe Text -> Maybe Text -> CrawlerConfig))
fetchParser = flip statement stmt
  where
    stmt = Statement sql (E.param E.int2) (D.rowMaybe decoder) True
    decoder = makeCrawlerConfig
      <$> D.nullableColumn D.text
      <*> D.column D.text
    sql = "SELECT text_hook, start_hook FROM parsers WHERE id=$1"

-- Only used by the test suite
fetchFragments :: Int32 -> Session [(Int, Int, Text)]
fetchFragments = flip statement stmt
  where
    stmt = Statement sql (E.param E.int4) (D.rowList decoder) True
    decoder = (,,)
      <$> (fromIntegral <$> D.column D.int4)
      <*> (fromIntegral <$> D.column D.int4)
      <*> D.column D.text
    sql = "SELECT ord, subord, fragment FROM page_fragments \
          \WHERE cid=$1 ORDER BY ord, subord"

-- Only used by the test suite
fetchBookmarks :: Int32 -> Session [(Int, Int, Int)]
fetchBookmarks = flip statement stmt
  where
    stmt = Statement sql (E.param E.int4) (D.rowList decoder) True
    col = fromIntegral <$> D.column D.int4
    decoder = (,,) <$> col <*> col <*> col
    sql = "SELECT uid, ord, subord FROM subscriptions \
          \WHERE cid=$1 ORDER BY uid"

-- Only used by the test suite
fetchMaxOrd :: Int32 -> Session (Int, Int)
fetchMaxOrd = flip statement stmt
  where
    stmt = Statement sql (E.param E.int4) (D.singleRow decoder) True
    col = D.column $ fromIntegral <$> D.int4
    decoder = (,) <$> col <*> col
    sql = "SELECT ord, subord FROM max_update_ord WHERE cid=$1"
