{-# LANGUAGE OverloadedStrings #-}

module Crawler.DB.Cache where

import Control.Monad.Trans.Except
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session
import Hasql.Statement

import Crawler.DB

precacheUserStats
  :: DBConfig
  -> IO ()
precacheUserStats settings = either (print . show) return =<<
  -- No transaction to minimize the amount of per user locks
  (runExceptT $ withConn settings $
    (statement () $ Statement
     "SELECT uid FROM users WHERE countme"
     E.unit (D.rowVector (D.column D.int4)) True) >>=
    (mapM_ (flip statement updateCache)))
  where
    updateCache = Statement
      "INSERT INTO comic_remain_frag_cache \
      \SELECT $1, cid, num FROM comic_remain_frag($1) JOIN comics USING (cid) \
      \ON CONFLICT (uid, cid) DO UPDATE SET num=EXCLUDED.num"
      (E.param E.int4) D.unit True
