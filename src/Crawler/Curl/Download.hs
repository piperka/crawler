{-# LANGUAGE OverloadedStrings #-}

module Crawler.Curl.Download where

import Control.Lens
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as B
import Data.Char (isAscii, isSpace)
import Data.IP (isMatchedTo, makeAddrRange)
import Data.Maybe (listToMaybe)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import qualified Data.Text.ICU as ICU
import Network.DNS.Lookup
import Network.DNS.Resolver
import Network.URI.Encode
import System.Exit
import System.Process hiding (readCreateProcessWithExitCode)
import System.Process.ByteString.Lazy

import Crawler.Types

downloadPage :: CrawlerConfig -> CrawlTarget -> IO (Either CrawlError ByteString)
downloadPage cc tgt = runExceptT $ do
  let host = fmap encodeUtf8 . ICU.group 1 =<< ICU.find hostRegex (tgt ^. nextURL)
  rs <- liftIO $ makeResolvSeed defaultResolvConf
  isTumblr <- maybe (return False)
    (\h -> liftIO $ withResolver rs $ \resolver ->
        either (const False)
        (maybe False (`isMatchedTo` tumblrRange) . listToMaybe) <$>
        lookupA resolver h) host
  let cfg = System.Process.proc "curl" $
        (if tgt ^. noContent then ("-I":) else id)
        $ (maybe id (\x -> (["-d", T.unpack x]<>)) $ tgt ^. postParams)
        $ ((concatMap (\(k, v) -> ["-H", (T.unpack k) <> ": " <> (T.unpack v)]) $
            tgt ^. extraHeaders) <>)
        $ (if isTumblr then id else (["-A", "piperka.net/1.0"] <>))
        $ (if disableCookies cc then id
           else maybe
                (["-b", "/tmp/crawler.cookies", "-c", "/tmp/crawler.cookies"] <>)
                (\fxd -> (["-b", T.unpack fxd] <>)) $ fixedCookies cc
          )
        [ "-i"
        , "-s"
        , "-m", "60"
        , "--retry", "2"
        , "--compressed"
        , "--max-filesize", "10000000"
        , T.unpack $ encodeTextWith ((&&) <$> isAscii <*> not . isSpace) $ tgt ^. nextURL
        ]
  (code, out, err) <- liftIO $ readCreateProcessWithExitCode cfg B.empty
  case code of
    ExitSuccess -> return out
    ExitFailure c -> throwE $ CurlFailure c err
  where
    hostRegex = ICU.regex [] "^https?://([^/:]+)"
    tumblrRange = makeAddrRange "66.6.32.0" 20
