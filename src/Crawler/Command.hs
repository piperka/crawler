{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

-- Support for running crawler via a serial protocol

module Crawler.Command where

import Control.Concurrent
import Control.Exception
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Except
import Control.Lens
import Data.Int
import qualified Data.IntMap as IntMap
import qualified Data.IntSet as IntSet
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Vector as V
import Hasql.Session hiding (run)

import Crawler.AutoDiscover
import Crawler.Crawl
import Crawler.Command.Query
import Crawler.Command.Types
import Crawler.DB hiding (withConn')
import Crawler.DB.Fetch
import Crawler.DB.Store
import Crawler.Types
import Crawler.Helpers.GoComics
import Crawler.Parser.Perl.Comm

crawlerApp :: ProgramConfig -> CrawlComm -> IO ()
crawlerApp crawlerCfg conn = bracket (startParser crawlerCfg) closeParser $ \pHandle -> do
  forever $ do
    msg <- receiveData conn
    let withConn' = withConn (crawlerCfg ^. dbConfig)
        withConnTransactionally' = withConnTransactionally (crawlerCfg ^. dbConfig)
        starter cfg = do
          adjust conn $ (((initialArchive . cConfig) .~ cfg) . (parserId .~ (parserType msg)))
          archive <- IntMap.fromList . pure . (1,) . view initialArchive <$> getCrawl conn
          resultVar <- crawl' crawlerCfg archive pHandle $ \cid ord ev -> do
            stop <- (^. crawlHalt) <$> (liftIO $ getCrawl conn)
            noEnd <- gets $ isNothing . (^. endCondition) . (IntMap.! 1)
            when stop $
              modify $ IntMap.adjust (endCondition .~ Just EndRequest) cid
            case ev of
              ParseEvent (Result p _) _ -> liftIO $
                (not . V.elem p . view (initialArchive . archivePages)  <$> getCrawl conn) >>=
                (flip when $ sendData conn $ CrawlAddPage ord $ Just p)
              ParseEvent (Multi v) _ ->
                liftIO $ mapM_ (sendData conn . uncurry CrawlAddPage) $
                zip [ord..] $ map Just $ reverse $ V.toList v
              TerminalEvent _ _ _ -> do
                gets (view newestPagePurge . (IntMap.! 1)) >>= flip when
                  (liftIO $ sendData conn $ CrawlRemovePage (ord-1))
              _ -> return ()
            when noEnd $ liftIO $ sendData conn $ CrawlProgress ord ev
          void $ forkIO $ takeMVar resultVar >>=
            \res -> do
              let r = res IntMap.! 1
              adjust conn $ crawlResult .~ Just r
              sendData conn $ CrawlDone r
    case msg of
      CmdStart pt cfgParams -> do
        adjust conn $ (crawlHalt .~ False) . flushResults
        (runExceptT . withConn' $ (fetchParser pt)) >>=
          (either
           (sendData conn . CrawlErr . T.pack . show)
           (maybe
            (sendData conn $ CrawlErr "no such parser")
            (starter . (cfgParams $))
           )
          )
      CmdStop -> adjust conn $ crawlHalt .~ True
      CmdReset -> do
        adjust conn reset
        sendData conn $ CrawlReset
      CmdAddPages pages -> do
        adjust conn $
          (initialArchive . archivePages %~ ((V.fromList $ reverse pages)<>)) . flushResults
        len <- V.length . view (initialArchive . archivePages) <$> getCrawl conn
        mapM_ (sendData conn . uncurry CrawlAddPage) $
          reverse $ zip [len-1,len-2..] $ reverse $ map Just pages
      CmdSave cid bookmoves homepage fixedHead -> do
        crawlState <- getCrawl conn
        maybe
          (sendData conn $ CrawlErr "no crawl done")
          (\res -> do
              x <- runExceptT . withConnTransactionally' $ do
                prepareMassUpdate
                let count = crawlState ^. (initialArchive . pageCount)
                deleteExistingPages cid count
                moveBookmarks cid bookmoves
                statement (zip3 (repeat cid) [((fromIntegral count)::Int32)..] .
                           map Just . drop count . reverse $ V.toList $
                           crawlState ^. (initialArchive . archivePages)) savePages
                saveCrawl $ IntMap.fromList [(fromIntegral cid, res)]
                makeDeletions cid . IntSet.toDescList $ crawlState ^. deletions
                updateComicParams cid homepage
                  (if T.null fixedHead then Nothing else Just fixedHead)
                  (crawlState ^. parserId) $
                  res ^. originalArchive ^. cConfig
                statement (fromIntegral cid) updateMax
              sendData conn $ either
                (CrawlErr . T.pack . show)
                (const $ CrawlAck "success") x
          ) $ crawlState ^. crawlResult
      CmdRemovePage ord -> do
        adjust conn $ (deletePage ord) . flushResults
        onlyMark <- (>ord) . view (initialArchive . pageCount) <$> getCrawl conn
        sendData conn $ (if onlyMark then CrawlMarkPageRemoval else CrawlRemovePage) ord
      CmdCrop ord -> do
        adjust conn $ (crop ord) . flushResults
        sendData conn $ CrawlCrop ord
      CmdDiscover ub ut source target -> do
        (autoDiscover crawlerCfg ub ut source target) >>=
          (either
           (sendData conn . CrawlErr . T.pack . show)
           (sendData conn . CrawlDiscover))
      CmdGoComics url -> goComicsHelper url >>=
        (helperResult $
         \(homepage, pages) ->
           sendData conn (CrawlGoComicsParams homepage) >> return pages)
      _ -> return ()
  where
    -- Move any existing crawlResult to initialArchive.  TODO: This
    -- discards fragments.
    flushResults c =
      let new = maybe mempty (view newPages) $ c ^. crawlResult
      in initialArchive . archivePages %~
         (\v -> V.fromList (filter (not . flip V.elem v) new) <> v) $
         crawlResult .~ Nothing $ c
    reset =
      (crawlResult .~ Nothing) .
      (initialArchive . archivePages .~ mempty) .
      (initialArchive . pageCount .~ 0) .
      (deletions .~ mempty)
    deletePage ord c
      | ord < c ^. initialArchive . pageCount =
          deletions %~ (IntSet.insert ord) $ c
      | otherwise =
          let dropOrd v = let (xs,ys) = V.splitAt ord v in xs <> V.drop 1 ys
          in initialArchive . archivePages %~ dropOrd $ c
    crop ord c
      | ord == 0 = reset c
      | ord < length (c ^. initialArchive . archivePages) =
          deletions %~ (IntSet.filter (< ord)) $
          initialArchive . pageCount %~ min ord $
          initialArchive . archivePages %~
          (\v -> V.slice (V.length v-ord) ord v) $ c
      | otherwise = c
    helperResult f = either (sendData conn . CrawlErr . T.pack) $
      f >=>
      mapM_ (\(ord, pg) -> sendData conn $ CrawlProgress ord
                           (ParseEvent (Result pg V.empty)
                            (CrawlTarget False Nothing [] False T.empty))) .
      zip [0..] . V.toList
