{-# LANGUAGE OverloadedStrings #-}

module Crawler.AutoDiscover.Query where

import Data.Text (Text)
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session hiding (sql)
import Hasql.Statement

import Crawler.Types

queryAll :: Statement () [(Int, Text -> Text -> Maybe Text -> Maybe Text -> CrawlerConfig)]
queryAll = Statement sql E.unit (D.rowList decoder) True
  where
    decoder = (,)
      <$> (fromIntegral <$> D.column D.int4)
      <*> (makeCrawlerConfig
           <$> D.nullableColumn D.text
           <*> D.column D.text)
    sql = "SELECT id, text_hook, start_hook FROM parsers ORDER BY id"

fetchAll :: Session [(Int, Text -> Text -> Maybe Text -> Maybe Text -> CrawlerConfig)]
fetchAll = statement () queryAll
