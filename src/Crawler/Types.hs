{-# LANGUAGE OverloadedStrings, RecordWildCards, ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}

module Crawler.Types where

import Control.Applicative ((<|>))
import Control.Lens hiding ((.=))
import Data.Aeson
import Data.Aeson.Types (Pair)
import qualified Data.ByteString
import Data.ByteString.Lazy (ByteString)
import Data.Default.Class
import Data.Int
import Data.IntMap (IntMap)
import Data.Maybe (isJust)
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8', encodeUtf8)
import qualified Data.Text.ICU as ICU
import Data.Vector (Vector, empty)
import Hasql.Connection (Settings)
import System.IO (FilePath)

data DBConfig = DBConfig Settings Data.ByteString.ByteString

data ProgramConfig = ProgramConfig
  { _crawlThreads :: Int
  , _parserDir :: FilePath
  , _dbConfig :: DBConfig
  }

makeLenses ''ProgramConfig

instance Default ProgramConfig where
  def = ProgramConfig 5 "." (DBConfig "postgresql://kaol@/piperka" "piperka")

data CrawlerConfig = CrawlerConfig
  { textHook :: Maybe Text
  , startHook :: Text
  , allowFile :: Bool
  , disableCookies :: Bool
  , firstNoContent :: Bool
  , fixedCookies :: Maybe Text
  , urlBase :: Text
  , urlTail :: Text
  , extraURL :: Maybe Text
  , extraData :: Maybe Text
  } deriving (Eq, Show, Read)

makeCrawlerConfig :: Maybe Text -> Text -> Text -> Text -> Maybe Text -> Maybe Text -> CrawlerConfig
makeCrawlerConfig textH startH = let
  rg = ICU.regex [ICU.Multiline] "^#\\s+(rss|url):"
  rg2 = ICU.regex [ICU.Multiline] "^#\\s+no_cookies"
  rg3 = ICU.regex [ICU.Multiline] "^#\\s*nocontent"
  rg4 = ICU.regex [ICU.Multiline] "^#\\s*cookies (.+)$"
  fileAllowable = isJust $ ICU.find rg startH
  noCookies = isJust $ ICU.find rg2 startH
  noCnt = isJust $ ICU.find rg3 startH
  fxd = ICU.group 1 =<< ICU.find rg4 startH
  in CrawlerConfig textH startH fileAllowable noCookies noCnt fxd

instance ToJSON CrawlerConfig where
  toJSON CrawlerConfig{..} = object
    [ "text_hook" .= textHook
    , "start_hook" .= startHook
    , "url_base" .= urlBase
    , "url_tail" .= urlTail
    , "extra_url" .= extraURL
    , "extra_data" .= extraData
    ]

instance FromJSON CrawlerConfig where
  parseJSON = let
    in withObject "CrawlerConfig" $ \v -> makeCrawlerConfig
    <$> v .:? "text_hook"
    <*> v .: "start_hook"
    <*> v .: "url_base"
    <*> v .: "url_tail"
    <*> v .:? "extra_url"
    <*> v .:? "extra_data"

instance Default CrawlerConfig where
  def = CrawlerConfig Nothing "" False False False Nothing "" "" Nothing Nothing

data CrawlTarget = CrawlTarget
  { _noContent :: Bool
  , _postParams :: Maybe Text
  , _extraHeaders :: [(Text, Text)]
  , _headRedir :: Bool
  , _nextURL :: Text
  } deriving (Show, Eq)

instance Default CrawlTarget where
  def = CrawlTarget False Nothing [] False ""

makeLenses ''CrawlTarget

instance ToJSON CrawlTarget where
  toJSON CrawlTarget{..} = object .
    (if _headRedir then ("head_redir" .= True:) else id) $
    [ "no_content" .= _noContent
    , "post_params" .= _postParams
    , "extra_headers" .= _extraHeaders
    , "next_url" .= _nextURL
    ]

instance FromJSON CrawlTarget where
  parseJSON = withObject "CrawlTarget" $ \v -> CrawlTarget
    <$> v .: "no_content"
    <*> v .: "post_params"
    <*> v .: "extra_headers"
    <*> (v .: "head_redir" <|> pure False)
    <*> v .: "next_url"

data CrawlError = FileForbidden | CurlFailure Int ByteString
  deriving (Show, Read, Eq)

instance ToJSON CrawlError where
  toJSON FileForbidden = object ["error" .= ("file_forbidden" :: String)]
  -- TODO
  toJSON (CurlFailure c _) = object
    ["error" .= ("curl" :: String), "code" .= c]

instance FromJSON CrawlError where
  parseJSON = withObject "CrawlError" $ \v -> v .: "error" >>= \(err :: Text) ->
    case err of
      "file_forbidden" -> return FileForbidden
      "curl" -> CurlFailure <$> v .: "code" <*> pure ""
      _ -> mempty

data ParseResult
  -- | Next page and possible fragments for current page
  = Result Text (Vector Text)
  -- | Archive page name pending, page resides on static archive head
  | AtFixedHead (Vector Text)
  -- | Multiple pages on one go, no fragments (most recent page last)
  | Multi (Vector Text)
  -- | No next link found, possible fragments
  | None (Vector Text)
  -- | Explicit stop requested by parser
  | Stop
  -- | Parser requests a new page for download
  | Fetch (Maybe Bool) CrawlTarget
  -- | Prospective new page checked and determined to not be a valid
  -- archive page after all
  | Faulty
  -- | HTTP error
  | ResponseError Int16 Text
  deriving (Show, Eq)

objectWithType :: String -> [Pair] -> Value
objectWithType typ xs = object $ ("type" .= typ):xs

instance ToJSON ParseResult where
  toJSON (Result n fr) = objectWithType "result"
    ["next" .= n, "fragments" .= fr]
  toJSON (AtFixedHead fr) = objectWithType "at_fixed_head"
    ["fragments" .= fr]
  toJSON (Multi ns) = objectWithType "multi"
    ["pages" .= ns]
  toJSON (None fr) = objectWithType "none"
    ["fragments" .= fr]
  toJSON Stop = objectWithType "stop" []
  toJSON (Fetch auto tgt) = (\(Object o1) (Object o2) -> Object $ o1 <> o2)
    (objectWithType "fetch" $ maybe [] (\a -> ["auto" .= a]) auto) (toJSON tgt)
  toJSON Faulty = objectWithType "faulty" []
  toJSON (ResponseError c m) = objectWithType "error"
    ["code" .= c, "message" .= m]

instance FromJSON ParseResult where
  parseJSON = withObject "ParseResult" $ \v -> v .: "type" >>= \(typ :: Text) ->
    case typ of
      "result" -> Result <$> v .: "next" <*> v .: "fragments"
      "multi" -> Multi <$> v .: "pages"
      "at_fixed_head" -> AtFixedHead <$> (maybe empty id <$> v .:? "fragments")
      "none" -> None <$> (maybe empty id <$> v .:? "fragments")
      "stop" -> return Stop
      "fetch" -> Fetch <$>
        v .:? "auto" <*>
        (CrawlTarget
         <$> v .:? "no_content" .!= False
         <*> v .:? "post_params"
         <*> (maybe [] id <$> v .:? "extra_headers")
         <*> (v .: "head_redir" <|> pure False)
         <*> v .: "next_url")
      "faulty" -> return Faulty
      "error" -> ResponseError <$> v .: "code" <*> v .: "message"
      _ -> mempty

data ArchiveEntry = ArchiveEntry
  { _pageCount :: Int
  -- | Vector of pages in reverse order, minus the possible head crop
  -- and fixed head
  , _archivePages :: Vector Text
  , _headCropCount :: Int
  , _cConfig :: CrawlerConfig
  } deriving (Show, Read, Eq)

makeLenses ''ArchiveEntry

instance Default ArchiveEntry where
  def = ArchiveEntry 0 empty 0 def

type ArchiveState = IntMap ArchiveEntry

data CrawlEndCondition
  -- | Next page link is already present in archive
  = EndLoop Int
  -- | No next page indicated by parser
  | EndNoNext
  -- | Next accessed page is not a valid archive page
  | EndFaulty
  -- | Failsafe limit triggered
  | EndCutoff
  -- | Too many redirect requests by parser
  | EndRedirects
  -- | Invalid redirect request
  | EndInvalidRedirect
  -- | HTTP error
  | EndError Int16 Text
  -- | Error with crawler
  | EndCrawl CrawlError
  -- | Error with parser
  | EndParser String
  -- | End by user request
  | EndRequest
  deriving (Show, Read, Eq)

instance ToJSON CrawlEndCondition where
  toJSON (EndLoop pos) = objectWithType "loop" ["pos" .= pos]
  toJSON EndNoNext = objectWithType "no_next" []
  toJSON EndFaulty = objectWithType "faulty" []
  toJSON EndCutoff = objectWithType "cutoff" []
  toJSON EndRedirects = objectWithType "redirects" []
  toJSON EndInvalidRedirect = objectWithType "invalid_redirect" []
  toJSON (EndError c m) = objectWithType "http_error"
    ["code" .= c, "message" .= m]
  toJSON (EndCrawl e) = (\(Object o1) (Object o2) -> Object $ o1 <> o2)
    (toJSON e) (objectWithType "crawl" [])
  toJSON (EndParser m) = objectWithType "parser" ["message" .= m]
  toJSON EndRequest = objectWithType "user" []

instance FromJSON CrawlEndCondition where
  parseJSON = withObject "CrawlEndCondition" $ \v -> v .: "type" >>= \(typ :: Text) ->
    case typ of
      "loop" -> EndLoop <$> v .:? "pos" .!= 0
      "no_next" -> return EndNoNext
      "faulty" -> return EndFaulty
      "cutoff" -> return EndCutoff
      "redirects" -> return EndRedirects
      "invalid_redirect" -> return EndInvalidRedirect
      "http_error" -> EndError <$> v .: "code" <*> v .: "message"
      "crawl" -> EndCrawl <$> parseJSON (Object v)
      "parser" -> EndParser <$> v .: "message"
      "user" -> return EndRequest
      _ -> mempty

data SiteState = SiteState
  { _newPages :: [Text]
  , _addFixedHead :: Bool
  , _fragments :: [Vector Text]
  , _originalArchive :: ArchiveEntry
  , _redirects :: Int
  , _newestPagePurge :: Bool
  , _replacingHead :: Bool
  , _endCondition :: Maybe CrawlEndCondition
  } deriving (Show, Read, Eq)

-- TODO: Add originalArchive
instance ToJSON SiteState where
  toJSON ss = object [ "newPages" .= _newPages ss
                     , "addFixedHead" .= _addFixedHead ss
                     , "fragments" .= _fragments ss
                     , "redirects" .= _redirects ss
                     , "newestPagePurge" .= _newestPagePurge ss
                     , "replacingHead" .= _replacingHead ss
                     , "endCondition" .= _endCondition ss
                     ]

makeLenses '' SiteState

-- Not using instance Monoid SiteState since using this as the unit
-- element would not be idempotent.
instance Default SiteState where
  def = SiteState [] False []
    def 0 False False Nothing

type CrawlState = IntMap SiteState

data Compensate
  = RemoveLoop Int Int
  | ReversedLoop Int Int
  | NoNextLoop
  | NoMatchForNext Text
  deriving (Show, Eq)

instance ToJSON Compensate where
  toJSON (RemoveLoop o e) = objectWithType "remove_loop"
    ["pos" .= o, "end" .= e]
  toJSON (ReversedLoop o e) = objectWithType "reversed_remove_loop"
    ["pos" .= o, "end" .= e]
  toJSON NoNextLoop = objectWithType "no_next_loop" []
  toJSON (NoMatchForNext u) = objectWithType "no_match_for_next"
    ["url" .= u]

instance FromJSON Compensate where
  parseJSON = withObject "Compensate" $ \v -> v .: "type" >>= \(typ :: Text) ->
    case typ of
      "remove_loop" -> RemoveLoop <$> v .: "pos" <*> v .: "end"
      "reversed_remove_loop" -> ReversedLoop <$> v .: "pos" <*> v .: "end"
      "no_next_loop" -> return NoNextLoop
      "no_match_for_next" -> NoMatchForNext <$> v .: "url"
      _ -> mempty

data CrawlEvent
  = TargetEvent CrawlTarget
  | ParseEvent ParseResult CrawlTarget
  | TerminalEvent CrawlEndCondition Int (Maybe ParseResult)
  | CompensateEvent Compensate
  deriving (Show, Eq)

instance ToJSON CrawlEvent where
  toJSON (TargetEvent tgt) = object
    ["target" .= toJSON tgt]
  toJSON (ParseEvent p tgt) = object
    ["target" .= toJSON tgt, "parse" .= toJSON p]
  toJSON (TerminalEvent end hc p) = object
    ((maybe id (\x -> (("parse" .= x):)) p)
     ["end" .= toJSON end, "head_crop" .= hc])
  toJSON (CompensateEvent c) = object
    ["compensate" .= toJSON c]

instance FromJSON CrawlEvent where
  parseJSON = withObject "CrawlEvent" $ \v ->
    (ParseEvent <$> v .: "target" <*> v .: "parse")
    <|> (TerminalEvent <$> v .: "end" <*> v .: "head_crop" <*> v .:? "target")
    <|> (TargetEvent <$> v .: "target")
    <|> (CompensateEvent <$> v .: "compensate")

data AuditError
  = TooFew
  | MismatchPage Int (Vector Text) (Vector Text)
  | NoNext Int Text (Maybe CrawlEndCondition)
  | DBError Data.ByteString.ByteString
  deriving (Show, Eq)

instance ToJSON AuditError where
  toJSON TooFew = objectWithType "too_few" []
  toJSON (MismatchPage ord orig got) = objectWithType "mismatch"
    ["ord" .= ord, "orig" .= orig, "got" .= got]
  toJSON (NoNext ord page end) = objectWithType "no_next"
    ["ord" .= ord, "page" .= page, "end" .= end]
  toJSON (DBError x) = objectWithType "db_error"
    ["error" .= either (const Nothing) Just (decodeUtf8' x)]

instance FromJSON AuditError where
  parseJSON = withObject "AuditError" $ \v -> v .: "type" >>= \(typ :: Text) ->
    case typ of
      "too_few" -> return TooFew
      "mismatch" -> MismatchPage <$> v .: "ord" <*> v .: "orig" <*> v .: "got"
      "no_next" -> NoNext <$> v.: "ord" <*> v .: "page" <*> v .: "end"
      "db_error" -> DBError <$> (encodeUtf8 <$> v .: "error")
      _ -> mempty
