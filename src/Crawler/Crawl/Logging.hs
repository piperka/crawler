module Crawler.Crawl.Logging where

import Control.Concurrent
import Control.Exception
import Control.Lens
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Control.Monad.Trans.State
import Data.ByteString (ByteString)
import Data.ByteString.UTF8 (fromString)
import qualified Data.IntMap as IntMap
import Data.IORef
import qualified Database.Memcache.Client as M
import Data.Semigroup ((<>))

import Crawler.Crawl
import Crawler.DB
import Crawler.DB.Log
import Crawler.Parser.Perl.Comm
import Crawler.Types

-- | Non-interactive logging crawl with cutoff
crawlLog :: ProgramConfig -> ArchiveState -> Int -> Bool
         -> IO (Either ByteString CrawlState)
crawlLog cfg archive cutoff verbose = runExceptT $
  withConn' (cfg ^. dbConfig) $ \conn ->
  bracket ((,) <$> startParser cfg <*> M.newClient [M.def] M.def)
  (\(b, mc) -> do
      closeParser b
      void $ M.delete mc updateStatusKey 0
      M.quit mc) $
  \(pHandle, mc) -> runExceptT $ do
  counter <- liftIO $ newIORef (0 :: Int)
  let setUpdate :: Int -> IO ()
      setUpdate i = void $ M.set mc updateStatusKey
        (fromString $
         "The update crawler is at work (" <> show i
         <> "/" <> show (IntMap.size archive) <> ")") 0 0
  liftIO $ setUpdate 0
  runId <- getRunId conn
  let control cid ord ev = do
        liftIO $ do
          case ev of
            ParseEvent _ _ -> modifyIORef counter (+1)
            _ -> return ()
          setUpdate =<< readIORef counter
        let cut = ord - archive IntMap.! cid ^. pageCount >= cutoff
            hc = archive IntMap.! cid ^. headCropCount
        when cut $
          modify $ IntMap.adjust (endCondition .~ Just EndCutoff) cid
        let ev' = if not cut then ev else
              TerminalEvent EndCutoff hc $
              case ev of
                ParseEvent p' _ -> Just p'
                TerminalEvent _ _ p' -> p'
                _ -> Nothing
        void $ liftIO $ runExceptT (logEvent runId (cid, ord, ev') conn)
        when verbose $ liftIO $ print (cid, ord, ev)
  res <- liftIO $ takeMVar =<< crawl' cfg archive pHandle control
  logToErrors runId conn
  return res
  where
    updateStatusKey = let DBConfig _ schema = cfg ^. dbConfig in
      schema <> fromString "-updatestatus"
