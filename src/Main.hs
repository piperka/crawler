{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Main where

import Control.Lens hiding (argument)
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import qualified Data.IntMap as IntMap
import Data.Maybe
import Data.Semigroup ((<>))
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Vector as V
import Hasql.Session (statement)
import Options.Applicative

import Crawler.AutoDiscover
import Crawler.Audit
import Crawler.Compensations
import Crawler.Crawl.Interactive
import Crawler.Crawl.Logging
import Crawler.DB
import Crawler.DB.Audit
import Crawler.DB.Cache
import Crawler.DB.Fetch
import Crawler.DB.Log
import Crawler.DB.Store
import Crawler.Types

data CrawlerOptions = SingleCrawl
  { single :: Int
  , updateStmp :: Bool
  , debugSingle :: Maybe String
  } | HourlyCrawl
  { verboseHourly :: Bool
  , cutoff :: Int
  , debugHourly :: Maybe String
  } | NewCrawl
  { newCid :: Maybe Int
  , newExtraURL :: Maybe Text
  , newExtraData :: Maybe Text
  , crop :: Int
  , debugNew :: Maybe String
  , parserId :: Int
  , urlBase :: Text
  , firstPageOrTail :: Text
  , maybeUrlTail :: Maybe Text
  } | AutoDiscoverCrawl
  { urlBase :: Text
  , firstPage :: Text
  , urlTail :: Text
  , targetPage :: Text
  } | AuditComics
  { auditCid :: Maybe Int
  } | FixUps
  { dryRun :: Bool
  , fixVerbose :: Bool
  , fixCid :: Maybe Int
  } deriving (Show)

dumpCrawlOption :: Parser (Maybe String)
dumpCrawlOption = optional $ strOption
  (long "dump"
   <> short 'd'
   <> help "dump crawl state"
  )

crawlerOptionsParser :: Parser CrawlerOptions
crawlerOptionsParser = subparser $
  (command "single" $
   flip info (progDesc "Crawl a single comic") $
   SingleCrawl
   <$> argument auto (metavar "CID"
                      <> help "Crawl a single comic"
                     )
   <*> switch (long "update"
               <> short 'u'
               <> help "Update last updated timestamp on success"
              )
   <*> dumpCrawlOption
  )
  <>
  (command "hourly" $
   flip info (progDesc "Full hourly cycle") $
   HourlyCrawl
   <$> switch (long "verbose"
               <> short 'v'
              )
   <*> option auto (long "cutoff"
                    <> short 'c'
                    <> value 50
                    <> help "Stop crawl after this many pages (default 50)"
                   )
   <*> dumpCrawlOption
  )
  <>
  (command "new" $
   flip info (progDesc "Insert a new comic") $
   NewCrawl
   <$> (optional $ option auto (long "cid"
                                <> short 'c'
                               ))
   <*> (optional $ T.pack <$> strOption (long "extraURL"
                                         <> short 'u'
                                        ))
   <*> (optional $ T.pack <$> strOption (long "extraData"
                                         <> short 'D'
                                        ))
   <*> (option auto (long "crop"
                     <> short 'k'
                     <> value 0
                    ))
   <*> dumpCrawlOption
   <*> argument auto (metavar "PARSER")
   <*> (T.pack <$> strArgument (metavar "URL_BASE"))
   <*> (T.pack <$> strArgument (metavar "FIRST_PAGE_OR_TAIL"))
   <*> (optional $ T.pack <$> strArgument (metavar "URL_TAIL"))
  )
  <>
  (command "auto" $
   flip info (progDesc "Automatically find a matching parser") $
   AutoDiscoverCrawl
   <$> (T.pack <$> strArgument (metavar "URL_BASE"))
   <*> (T.pack <$> strArgument (metavar "FIRST_PAGE"))
   <*> (T.pack <$> strArgument (metavar "URL_TAIL"))
   <*> (T.pack <$> strArgument (metavar "TARGET_PAGE"))
  )
  <>
  (command "audit" $
   flip info (progDesc "Audit comics") $
   AuditComics
   <$> (optional $ argument auto (metavar "CID"
                                  <> help "Audit a single comic"
                                 ))
  )
  <>
  (command "fix" $
   flip info (progDesc "Fix crawl issues") $
   FixUps
   <$> switch (long "dry"
               <> short 'd'
              )
   <*> switch (long "verbose"
               <> short 'v'
              )
   <*> (optional $ option auto (long "cid"
                                <> short 'c'
                               ))
  )

main :: IO ()
main = do
  uncurry crawler =<< execParser opts
  where
    opts =
      info (
      ((,)
       <$> (ProgramConfig
            <$> option auto (long "threads" <> value 5)
            <*> option str (long "parserDir" <> value "/srv/piperka.net")
            <*> (DBConfig
                 <$> option str (long "db" <> value "postgresql://kaol@/piperka")
                 <*> option str (long "schema" <> value "piperka")))
       <*> crawlerOptionsParser) <**> helper)
           (fullDesc
            <> progDesc "Piperka Crawler"
           )

crawler :: ProgramConfig -> CrawlerOptions -> IO ()
crawler settings (SingleCrawl cid updateStamp debug) = do
  res <- runExceptT $ do
    archive <- maybe (throwE "no such comic") return =<<
               (withConn (settings ^. dbConfig) $ fetchSingleArchive True $ fromIntegral cid)
    state <- lift $ crawlInteractive settings archive
    maybe (return ()) (lift . uncurry writeFile) $
      (,) <$> debug <*> (show <$> state)
    maybe
      (return ())
      (\state' -> withConnTransactionally (settings ^. dbConfig) $ do
          let updated = updateCounts archive state'
          saveCrawl state'
          when updateStamp $
            statement (map fromIntegral $ IntMap.keys updated) updateLastUpdated
      )
      state
  putStrLn $ show res

crawler settings (HourlyCrawl verbose ct debug) = do
  res <- runExceptT $ do
    archive <- withConn (settings ^. dbConfig) fetchHourly
    state <- ExceptT $ crawlLog settings archive ct verbose
    let updated = updateCounts archive state
    maybe (return ()) (lift . flip writeFile (show state)) debug
    withConnTransactionally (settings ^. dbConfig) $ do
      saveCrawl state
      let updatedCids = map fromIntegral $ IntMap.keys updated
      statement updatedCids updateLastUpdated
      saveCrawlScores updatedCids
    liftIO $ precacheUserStats (settings ^. dbConfig)
    when verbose $ liftIO $ print updated
    return ()
  either print (const $ return ()) res

crawler settings (NewCrawl c eURL eData cr debug pId ub fpot mut) = do
  let (fp, ut) = maybe (Nothing, fpot) (\x -> (Just fpot, x)) mut
  res <- runExceptT $ do
    cid <- maybe (withConn (settings ^. dbConfig) fetchNewCid) (return . fromIntegral) c
    parser <- (withConn (settings ^. dbConfig) $ fetchParser (fromIntegral pId)) >>=
      maybe (throwE "no such parser") (\x -> return $ x ub ut eURL eData)
    archive <- IntMap.map
      ((\v -> ArchiveEntry (V.length v) v 0 parser) .
       V.drop cr .
       (\v -> maybe v V.singleton (guard (V.length v <= 1) >> fp)) .
       V.mapMaybe id) <$>
      (withConn (settings ^. dbConfig) $ fetchPages' [cid])
    when (IntMap.null archive) $ throwE "no seed page"
    state <- lift $ crawlInteractive settings archive
    maybe (return ()) (lift . uncurry writeFile) $
      (,) <$> debug <*> (show <$> state)
    maybe
      (return ())
      (\state' -> withConnTransactionally (settings ^. dbConfig) $ do
          let a = archive IntMap.! (fromIntegral cid)
              updated = updateCounts archive state'
          statement [(cid
                     , fromIntegral $ a ^. pageCount - 1
                     , Just $ V.head $ a ^. archivePages
                     )] savePages
          saveCrawl state'
          statement (map fromIntegral $ IntMap.keys updated) updateLastUpdated
      ) state
  print res

crawler settings (AuditComics paramCid) = do
  res <- runExceptT $ runMaybeT $ do
    cid <- MaybeT $ maybe
      (withConn (settings ^. dbConfig) $ fmap fromIntegral <$> getAuditCandidate)
      (return . Just) paramCid
    lift $ do
      err <- liftIO $ auditComic settings cid
      withConn (settings ^. dbConfig) $ do
        when (isNothing paramCid) $ bumpAuditComic $ fromIntegral cid
        saveAuditResult (fromIntegral cid) err
      return (cid, err)
  print res

crawler settings (FixUps dry verbose cid) = do
  res <- runExceptT $ do
    events <- withExceptT show $ withConn (settings ^. dbConfig) $
      (maybe id (\cid' -> filter (\(c,_,_) -> c == fromIntegral cid')) cid) .
      either error (map (\(a,b,c) -> (a, fromIntegral b, c))) <$> getLogEvents
    actions <- catMaybes <$>
      (mapM (\(c, f) -> do
                aState <- withExceptT show $ withConn (settings ^. dbConfig) $
                          fetchSingleArchive True c
                liftIO $ maybe (return Nothing)
                  (f >=> (return . Just . (c,))) aState
            ) $ catMaybes $ map (\(c, ord, ev) -> (c,) <$> compensation ord ev) events)
    when (verbose || dry) $ liftIO $ print actions
    when (not dry) $ withExceptT show $ withConnTransactionally (settings ^. dbConfig) $
      mapM_ (uncurry compensate) actions
  either print (const $ return ()) res

crawler settings (AutoDiscoverCrawl ub fp ut tgt) = do
  print =<< autoDiscover settings ub ut fp tgt
