module Crawler.Test where

import System.IO.Unsafe (unsafePerformIO)
import Test.Tasty
import qualified Test.Tasty.Hspec as H

import Crawler.Snap
import Crawler.Test.Command
import Crawler.Test.Compensations
import Crawler.Test.Crawler
import Crawler.Test.DB
import Crawler.Test.ParseResult
import Crawler.Test.SimpleParse

main :: IO ()
main = do
  testServe $ defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [perlParserTests]

perlParserTests :: TestTree
perlParserTests = testGroup "Perl parser" $ map (unsafePerformIO $)
  [ H.testSpec "Parser process" perlProcessTests
  , H.testSpec "processResponse" parseResultTests
  , H.testSpec "Crawler" crawlerTests
  , H.testSpec "DB" dbTests
  , H.testSpec "Command" commandTests
  , H.testSpec "Compensations" compensationsTests
  ]
