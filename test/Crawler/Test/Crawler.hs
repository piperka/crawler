{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Crawler.Test.Crawler where

import Control.Exception
import Control.Lens
import Control.Monad (join)
import Data.Aeson
import qualified Data.ByteString.Lazy as B
import Data.Default.Class
import qualified Data.IntMap as IntMap
import Data.Maybe (isJust, fromJust)
import qualified Data.Text as T
import qualified Data.Vector as V
import System.Directory (getCurrentDirectory, removeFile)
import System.IO.Error
import Test.Hspec

import Crawler.Crawl
import Crawler.Types

crawlerTests :: Spec
crawlerTests = do
  it "can download a page and use parser on it" simpleCrawl
  it "detects error on invalid URLs" invalidURL
  it "stops crawling on cutoff" cutoffTest
  it "decompresses pages" decompress
  it "can decode percent encoding" percentEncodeTest
  it "can get updates from RSS" rssParse
  it "can get updates from RSS in file" rssParseFile
  it "can process fragments" fragmentsTest
  it "does next requests submitted by parser" requestNextTest
  it "marks 404 pages as faulty and doesn't store them" missingTest
  it "doesn't save looping pages" loopingTest
  it "sends POST data" $ requestPostTest "test/files/post_config.json"
  it "sends POST data, application/json" $ requestPostTest "test/files/post_json_config.json"
  it "fetches pages when all downloads redirect once" $ redirectingSiteTest
  it "can use extra_tags parser config" $ extraTagsTest

simpleCrawl :: IO ()
simpleCrawl = do
  cfg <- decode <$> B.readFile "test/files/simple_config.json"
  cfg `shouldSatisfy` isJust
  let archive = IntMap.fromList [(1, ArchiveEntry 0 (V.singleton "1") 0 $ fromJust cfg)]
  res <- crawl archive Nothing
  res `shouldBe`
    IntMap.fromList [(1, def &
                         originalArchive .~ archive IntMap.! 1 &
                         newPages .~ ["2"] &
                         fragments .~ replicate 2 V.empty &
                         endCondition .~ Just (EndLoop 0))]

invalidURL :: IO ()
invalidURL = do
  cfg <- decode <$> B.readFile "test/files/simple_config.json"
  cfg `shouldSatisfy` isJust
  let cfg' = (fromJust cfg) {urlBase = "nosuch://protocol/"}
      archive = IntMap.fromList [(1, ArchiveEntry 0 (V.singleton "1") 0 cfg')]
  res <- crawl archive Nothing
  res `shouldSatisfy` \x -> case x IntMap.! 1 ^. endCondition of
    Just (EndCrawl (CurlFailure _ _)) -> True
    _ -> False

cutoffTest :: IO ()
cutoffTest = do
  cfg <- decode <$> B.readFile "test/files/simple_config.json"
  cfg `shouldSatisfy` isJust
  let cfg' = (fromJust cfg) {urlBase = "http://localhost:8000/increasing?id="}
      archive = IntMap.fromList [(1, ArchiveEntry 0 (V.singleton "1") 0 cfg')]
      endPages = map (T.pack . show) ([11,10..2] :: [Int])
      endFragments = map (const V.empty) endPages
  res <- crawl archive $ Just 10
  res `shouldBe`
    IntMap.fromList [(1, def &
                         originalArchive .~ archive IntMap.! 1 &
                         newPages .~ endPages &
                         fragments .~ endFragments &
                         endCondition .~ Just EndCutoff)]

decompress :: IO ()
decompress = do
  cfg' <- fromJust . decode <$> B.readFile "test/files/simple_config.json"
  let cfg = cfg' {urlBase = "http://localhost:8000/compressed_page.html?id="}
      archive = IntMap.fromList [(1, ArchiveEntry 0 (V.singleton "1") 0 cfg)]
  res <- crawl archive Nothing
  res `shouldBe`
    IntMap.fromList [(1, def &
                         originalArchive .~ archive IntMap.! 1 &
                         newPages .~ ["2"] &
                         fragments .~ replicate 2 V.empty &
                         endCondition .~ Just (EndLoop 0))]

percentEncodeTest :: IO ()
percentEncodeTest = do
  cfg <- fromJust . decode <$> B.readFile "test/files/simple_html_config.json"
  let archive = IntMap.fromList [(1, ArchiveEntry 0 (V.singleton "percent1") 0 cfg)]
  res <- crawl archive Nothing
  res `shouldBe`
    IntMap.fromList [(1, def &
                         originalArchive .~ archive IntMap.! 1 &
                         newPages .~ ["percent2-ä"] &
                         fragments .~ replicate 2 V.empty &
                         endCondition .~ Just EndNoNext)]

rssParse :: IO ()
rssParse = do
  cfg <- fromJust . decode <$> B.readFile "test/files/rss_config.json"
  let archive = IntMap.fromList [(1, ArchiveEntry 0 (V.singleton "simple_page") 0 cfg)]
      endPages = ["asdf", "hello", "another_page", "page2"]
  res <- crawl archive Nothing
  res `shouldBe`
    IntMap.fromList [(1, def &
                         originalArchive .~ archive IntMap.! 1 &
                         newPages .~ endPages &
                         fragments .~ replicate 4 V.empty &
                         endCondition .~ Just EndNoNext)]

rssParseFile :: IO ()
rssParseFile = do
  cwd <- getCurrentDirectory
  cfg' <- fromJust . decode <$> B.readFile "test/files/rss_config.json"
  let cfg = cfg' { extraURL = Just $ T.pack $
                              "file://" <> cwd <> "/test/files/simple_rss.xml"
                 , startHook = T.replace
                               "http://localhost:8000/simple_rss.xml"
                               "somethingInvalid" $ startHook cfg'
                 }
      archive = IntMap.fromList [(1, ArchiveEntry 0 (V.singleton "simple_page") 0 cfg)]
      endPages = ["asdf", "hello", "another_page", "page2"]
  res <- crawl archive Nothing
  res `shouldBe`
    IntMap.fromList [(1, def &
                         originalArchive .~ archive IntMap.! 1 &
                         newPages .~ endPages &
                         fragments .~ replicate 4 V.empty &
                         endCondition .~ Just EndNoNext)]

fragmentsTest :: IO ()
fragmentsTest = do
  cfg <- fromJust . decode <$> B.readFile "test/files/fragments_config.json"
  let archive = IntMap.fromList [(1, ArchiveEntry 0 (V.singleton "page1") 0 cfg)]
      endPages = ["page3", "page2"]
      endFragments = map V.fromList
        [ ["frag3-1", "frag3-2", "frag3-3", "frag3-4", "frag3-5"]
        , ["frag2-1", "frag2-2", "frag2-3", "frag2-4", "frag2-5"]
        , ["frag1-1", "frag1-2", "frag1-3", "frag1-4", "frag1-5"]
        ]
  res <- crawl archive Nothing
  res `shouldBe`
    IntMap.fromList [(1, def &
                         originalArchive .~ archive IntMap.! 1 &
                         newPages .~ endPages &
                         fragments .~ endFragments &
                         endCondition .~ Just EndNoNext)]

requestNextTest :: IO ()
requestNextTest = do
  -- Test two variants of requests
  cfg <- fromJust . decode <$> B.readFile "test/files/redirects_config.json"
  cfg2 <- fromJust . decode <$> B.readFile "test/files/redirects_config2.json"
  let archive = IntMap.fromList
        [ (1, ArchiveEntry 0 (V.singleton "1/page") 0 cfg)
        , (2, ArchiveEntry 0 (V.singleton "1/page") 0 cfg2)
        ]
      endSiteState = def &
                     newPages .~ ["3/page", "2/page"] &
                     fragments .~ replicate 3 V.empty &
                     endCondition .~ Just EndNoNext
      endState = IntMap.fromList [ (1, endSiteState & originalArchive .~ archive IntMap.! 1)
                                 , (2, endSiteState & originalArchive .~ archive IntMap.! 2) ]
  res <- crawl archive Nothing
  res `shouldBe` endState

missingTest :: IO ()
missingTest = do
  cfg <- fromJust . decode <$> B.readFile "test/files/simple_html_config.json"
  let archive = IntMap.fromList [(1, ArchiveEntry 0 (V.singleton "missing1") 0 cfg)]
  res <- (IntMap.lookup 1) <$> crawl archive Nothing
  res `shouldSatisfy` (== Just ["missing2"]) . (fmap (^. newPages))
  res `shouldSatisfy` (== Just True) . (fmap (^. newestPagePurge))
  let isExpectedEnd (Just (EndError 404 _)) = True
      isExpectedEnd _ = False
  res `shouldSatisfy` isExpectedEnd . join . (fmap (^. endCondition))

loopingTest :: IO ()
loopingTest = do
  cfg <- fromJust . decode <$> B.readFile "test/files/simple_html_config.json"
  let archive = IntMap.fromList [(1, ArchiveEntry 0 (V.singleton "looping1") 0 cfg)]
  res <- crawl archive Nothing
  res `shouldBe`
    IntMap.fromList [(1, def &
                         originalArchive .~ archive IntMap.! 1 &
                         newestPagePurge .~ True &
                         newPages .~ ["looping2"] &
                         fragments .~ replicate 2 V.empty &
                         redirects .~ 10 &
                         endCondition .~ Just EndRedirects)]

requestPostTest :: String -> IO ()
requestPostTest cfgFile = do
  cfg <- fromJust . decode <$> B.readFile cfgFile
  let handleExists e
        | isDoesNotExistError e = return ()
        | otherwise = throwIO e
  removeFile "/tmp/crawler.cookies" `catch` handleExists
  let archive = IntMap.fromList [(1, ArchiveEntry 0 (V.singleton "page1") 0 cfg)]
  res <- crawl archive Nothing
  res `shouldBe`
    IntMap.fromList [(1, def &
                         originalArchive .~ archive IntMap.! 1 &
                         newPages .~ ["page3", "page2"] &
                         fragments .~ replicate 3 V.empty &
                         endCondition .~ Just EndNoNext)]

redirectingSiteTest :: IO ()
redirectingSiteTest = do
  cfg <- fromJust . decode <$> B.readFile "test/files/simple_config.json"
  let base = "http://localhost:8000/increasing?max=4&alwaysRedirect=1&id="
      archive = IntMap.fromList [(1, ArchiveEntry 0 (V.fromList ["3", "2", "1"]) 0
                                     (cfg { urlBase = base}))]
  res <- crawl archive Nothing
  res `shouldBe`
    IntMap.fromList [(1, def &
                         originalArchive .~ archive IntMap.! 1 &
                         replacingHead .~ True &
                         newPages .~ ["4", "3"] &
                         fragments .~ replicate 3 V.empty &
                         endCondition .~ Just (EndLoop 0))]

extraTagsTest :: IO ()
extraTagsTest = do
  cfg <- fromJust . decode <$> B.readFile "test/files/extra_tags/config.json"
  let archive = IntMap.fromList [(1, ArchiveEntry 0 (V.singleton "page1") 0 cfg)]
  res <- crawl archive Nothing
  res `shouldBe`
    IntMap.fromList [(1, def &
                         originalArchive .~ archive IntMap.! 1 &
                         newPages .~ ["page3", "page2"] &
                         fragments .~ replicate 3 V.empty &
                         endCondition .~ Just EndNoNext)]
