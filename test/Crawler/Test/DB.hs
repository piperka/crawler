{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE RankNTypes #-}

module Crawler.Test.DB where

import Control.Concurrent
import Control.Exception
import Control.Lens
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Aeson
import Data.ByteString (ByteString)
import qualified Data.ByteString.Lazy as B
import Data.Default.Class
import Data.IORef
import Data.Int
import qualified Data.IntMap as IntMap
import Data.Maybe
import Data.Time.Clock
import qualified Data.Vector as V
import Hasql.Session
import Test.Hspec

import qualified Crawler.Crawl
import Crawler.Crawl.Logging
import Crawler.DB
import Crawler.DB.Fetch
import Crawler.DB.Log (getLogErrors)
import Crawler.DB.Score
import Crawler.DB.Store
import Crawler.Parser.Perl.Comm
import Crawler.Test.DB.Init
import Crawler.Types

dbTests :: Spec
dbTests = before_ initDB $ do
  it "can fetch config and archive from DB" initFromDB
  it "can crawl and store a single page" simpleStore
  it "can crawl pages with fragments" fragmentCrawl
  it "can use head crop" headCrop
  it "can use fixed head" fixedHead
  it "can use RSS and extra_url for alternate location" rssTest
  it "can replace a redirecting/broken latest page" brokenHeadTest
  it "does full hourly cycle" hourlyCycle

crawl :: ArchiveState
      -> [(Int,Int)] -> ExceptT ByteString IO ()
crawl archive expected = do
  settings <- getSettings
  res <- lift $ Crawler.Crawl.crawl archive Nothing
  let updated = updateCounts archive res
  lift $ updated `shouldBe` IntMap.fromAscList expected
  withConnTransactionally settings $ do
    saveCrawl res
    statement (map fromIntegral $ IntMap.keys updated) updateLastUpdated

initFromDB :: IO ()
initFromDB = do
  settings <- getSettings
  cfg2 <- decode <$> B.readFile "test/files/simple_config.json"
  res <- runExceptT $ do
    cfg <- fmap snd <$> withConn settings (fetchSingle 1)
    lift $ cfg `shouldBe` cfg2
    lift $ cfg `shouldSatisfy` isJust
    archive <- withConn settings $ fetchPages [1]
    lift $ archive `shouldBe` IntMap.fromList [(1, (0, V.fromList ["1"]))]
    return ()
  res `shouldBe` Right ()

simpleStore :: IO ()
simpleStore = do
  settings <- getSettings
  res <- runExceptT $ do
    cfg <- fromJust <$> (withConn settings $ fetchSingle 1)
    archive <- IntMap.map (flip uncurry cfg . uncurry ArchiveEntry) <$>
      (withConn settings $ fetchPages [1])
    lift $ IntMap.map (view archivePages) archive `shouldBe`
      IntMap.fromList [(1, V.fromList ["1"])]
    crawl archive [(1,1)]
    (pagesAfter, maxOrd) <- withConn settings $
      (,) <$> fetchPages' [1] <*> fetchMaxOrd 1
    lift $ pagesAfter `shouldBe`
      IntMap.fromList [(1, V.fromList $ map Just ["2", "1"])]
    lift $ maxOrd `shouldBe` (1,1)
    return ()
  res `shouldBe` Right ()

fragmentCrawl :: IO ()
fragmentCrawl = do
  settings <- getSettings
  res <- runExceptT $ do
    cfg <- fromJust <$> (withConn settings $ fetchSingle 2)
    archive <- IntMap.map (flip uncurry cfg . uncurry ArchiveEntry) <$>
      (withConn settings $ fetchPages [2])
    lift $ (IntMap.map (view archivePages) archive) `shouldBe`
      IntMap.fromList [(2, V.fromList ["page1", "page0"])]
    crawl archive [(2,2)]
    pagesAfter <- withConn settings $ fetchPages' [2]
    lift $ pagesAfter `shouldBe`
      IntMap.fromList [(2, V.fromList $ map Just ["page3", "page2", "page1", "page0"])]
    (frags, maxOrd) <- withConn settings $
      (,) <$> fetchFragments 2 <*> fetchMaxOrd 2
    lift $ frags `shouldBe`
      [ (0,1,"frag0-1")
      , (1,1,"frag1-1"), (1,2,"frag1-2"), (1,3,"frag1-3")
      , (1,4,"frag1-4"), (1,5,"frag1-5")
      , (2,1,"frag2-1"), (2,2,"frag2-2"), (2,3,"frag2-3")
      , (2,4,"frag2-4"), (2,5,"frag2-5")
      , (3,1,"frag3-1"), (3,2,"frag3-2"), (3,3,"frag3-3")
      , (3,4,"frag3-4"), (3,5,"frag3-5")
      ]
    lift $ maxOrd `shouldBe` (3, 5)
    return ()
  res `shouldBe` Right ()

headCrop :: IO ()
headCrop = do
  settings <- getSettings
  res <- runExceptT $ do
    cfg <- fromJust <$> (withConn settings $ fetchSingle 3)
    archive <- IntMap.map (flip uncurry cfg . uncurry ArchiveEntry) <$>
      (withConn settings $ fetchPages [3])
    lift $ (IntMap.map (view archivePages) archive) `shouldBe`
      IntMap.fromList [(3, V.fromList ["page1"])]
    crawl archive [(3,1)]
    pagesAfter <- withConn settings $ fetchPages' [3]
    lift $ pagesAfter `shouldBe`
      IntMap.fromList [(3, V.fromList $ map Just ["page3", "page2", "page1"])]
    return ()
  res `shouldBe` Right ()

fixedHead :: IO ()
fixedHead = do
  settings <- getSettings
  res <- runExceptT $ do
    cfg <- fromJust <$> (withConn settings $ fetchSingle 4)
    archive <- IntMap.map (flip uncurry cfg . uncurry ArchiveEntry) <$>
      (withConn settings $ fetchPages [4])
    lift $ (IntMap.map (view archivePages) archive) `shouldBe`
      IntMap.fromList [(4, V.fromList ["page2", "page1"])]
    crawl archive [(4,1)]
    pagesAfter <- withConn settings $ fetchPages' [4]
    lift $ pagesAfter `shouldBe`
      IntMap.fromList [(4, V.fromList $ Nothing:(map Just ["page3", "page2", "page1"]))]
    return ()
  res `shouldBe` Right ()

rssTest :: IO ()
rssTest = do
  settings <- getSettings
  res <- runExceptT $ do
    cfgs <- zip [5,6] . map fromJust <$> mapM (withConn settings . fetchSingle) [5,6]
    archive <- IntMap.mapWithKey
               (\k -> ((flip uncurry (fromJust $ lookup k cfgs)) . uncurry ArchiveEntry)) <$>
               (withConn settings $ fetchPages [5,6])
    lift $ (IntMap.map (view archivePages) archive) `shouldBe`
      IntMap.fromList [ (5, V.fromList ["simple_page"])
                      , (6, V.fromList ["aaa"])
                      ]
    crawl archive [(5,4), (6,2)]
    pagesAfter <- withConn settings $ fetchPages' [5,6]
    lift $ pagesAfter `shouldBe`
      IntMap.fromList [ (5, V.fromList $ map Just
                          ["asdf", "hello", "another_page", "page2", "simple_page"])
                      , (6, V.fromList $ map Just $ ["ccc", "bbb", "aaa"])
                      ]
    return ()
  res `shouldBe` Right ()

brokenHeadTest :: IO ()
brokenHeadTest = bracket (startParser def) closeParser $ \prs -> do
  settings <- getSettings
  let cids = [11, 12]
      isHeadRedir (_, (ParseEvent _ tgt)) = tgt ^. headRedir
      isHeadRedir _ = False
  logsRef <- newIORef . IntMap.fromList $ map ((,[]) . fromIntegral) cids
  res <- runExceptT $ do
    let control cid ord ev = liftIO $ modifyIORef logsRef $
          IntMap.adjust ((ord,ev):) cid
    cfgs <- zip (map fromIntegral cids) .
            map fromJust <$> mapM (withConn settings . fetchSingle) cids
    archive <- IntMap.mapWithKey
               (\k -> ((flip uncurry (fromJust $ lookup k cfgs)) . uncurry ArchiveEntry)) <$>
               (withConn settings $ fetchPages cids)
    lift $ (IntMap.map (view archivePages) archive) `shouldBe`
      IntMap.fromList [ (11, V.fromList ["redirecting_page", "page1"])
                      , (12, V.fromList ["404page", "page1"])
                      ]
    res <- liftIO $ takeMVar =<< Crawler.Crawl.crawl' def archive prs control
    withConnTransactionally settings $ saveCrawl res
    logs <- liftIO $ readIORef logsRef
    lift $ do
      (updateCounts archive res) `shouldBe` (IntMap.fromAscList [(11, 1), (12, 1)])
      (map isHeadRedir <$> logs) `shouldBe`
        (IntMap.fromList [ (11, [False, False, False, True, False])
                         , (12, [False, False, False, True, False])
                         ])
    pagesAfter <- withConn settings $ fetchPages' cids
    lift $ pagesAfter `shouldBe`
      IntMap.fromList [ (11, V.fromList $ map Just ["page3", "page2", "page1"])
                      , (12, V.fromList $ map Just ["page3", "page2", "page1"])
                      ]
    return ()
  res `shouldBe` Right ()

hourlyCycle :: IO ()
hourlyCycle = do
  settings <- getSettings
  let programCfg = dbConfig .~ settings $ def
  -- Determined by update_score given in db_init.sql
  let toUpdate = [1,3,4,6,7,8,9,13] :: [Int32]
  stamp <- getCurrentTime
  res <- runExceptT $ do
    archive <- withConn settings $ fetchHourly
    lift $ IntMap.keys archive `shouldBe` map fromIntegral toUpdate
    -- TODO test for crawler_log contents
    res <- ExceptT $ crawlLog programCfg archive 50 False
    let updated = updateCounts archive res
        expected = IntMap.fromAscList [(1,1), (3,1), (4,1), (6,2)]
    lift $ updated `shouldBe` expected
    (pagesAfter, scores, errLog) <- withConnTransactionally settings $ (,,)
      <$ saveCrawl res
      <* saveCrawlScores (map fromIntegral $ IntMap.keys updated)
      <*> fetchPages' toUpdate
      <*> getScores ([1..9]<>[13])
      <*> getLogErrors
    lift $ pagesAfter `shouldBe` IntMap.fromList
      ((4,V.fromList [Nothing, Just "page3", Just "page2", Just "page1"]):
       (over (mapped._2) (V.fromList . map Just) $
        [ (1,["2","1"])
        , (3,["page3","page2","page1"])
        , (6,["ccc","bbb","aaa"])
        , (7,["page3","page2","page1"])
        , (8,["page3","page2","page1"])
        , (9,["invalid"])
        , (13,["page3","page2","page1"])
        ]))
    -- Timestamps should have been updated
    lift $ (map ((>stamp) . (^. _2 . _1)) $ IntMap.toList scores) `shouldBe`
      [True, False, True, True, False, True, False, False, False, False]
    -- Updated comics should have negative update_score
    lift $ (map ((< (-100)) . (^. _2 . _2)) $ IntMap.toList scores) `shouldBe`
      [True, False, True, True, False, True, False, False, False, False]
    -- A comic with no update found should have reduced update_score
    lift $ scores IntMap.! 7 `shouldSatisfy`
      (\(_,score,value) -> score < 5 && score > 0 && value < 1)
    -- A comic with elevated update_value should have it reduced
    lift $ scores IntMap.! 2 `shouldSatisfy`
      (\(_,score,value) -> score > 20 && value < 19.9 && value > 19)
    -- Updated comics should have elevated update_value
    lift $ (map ((> 10) . (^. _2 . _3)) $ IntMap.toList scores) `shouldBe`
      [True, True, True, True, False, True, False, False, False, False]
    -- Invalid page in archive should be recorded
    lift $ errLog `shouldBe` [(2, 9, 0, 404, "http://localhost:8000/invalid/invalid.html")]
    return ()
  res `shouldBe` Right ()
