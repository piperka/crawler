{-# LANGUAGE OverloadedStrings #-}

module Crawler.Test.ParseResult (parseResultTests) where

import Control.Lens
import Control.Monad.Trans.State
import Data.Aeson (decode)
import qualified Data.ByteString.Lazy as B
import Data.Default.Class
import qualified Data.IntMap as IntMap
import Data.Maybe
import Data.Text (Text)
import qualified Data.Vector as V
import Test.Hspec

import Crawler.Types
import Crawler.Parser.Perl.Actions

parseResultTests :: Spec
parseResultTests = do
  it "inserts a single new page" simpleInsert
  it "ends crawl on None" simpleDone
  it "purges head on faulty parse" simpleFaulty
  it "refuses loops with archive" simpleLoop
  it "refuses loops with new pages" loopInResults
  it "counts loop position in archive with newPages" loopInArchiveWithNewPages
  it "handles overlap in Multi parse result" overlapTest
  it "knows to stop crawl after static URL use" staticURLTest

getWorld :: IO ArchiveState
getWorld = do
  let archive = V.fromList ["4", "3", "2", "1"]
  cfg <- decode <$> B.readFile "test/files/simple_config.json"
  cfg `shouldSatisfy` isJust
  return $ IntMap.fromList [(1, ArchiveEntry (V.length archive) archive 0 (fromJust cfg))]

testState :: ArchiveEntry -> [Text] -> CrawlState
testState a xs = IntMap.fromList
  [(1, def & newPages .~ xs & originalArchive .~ a)]

simpleInsert :: IO ()
simpleInsert = do
  world <- getWorld
  let res = (1, Result "5" V.empty)
      initial = testState (world IntMap.! 1) []
      (a, s) = runState (parseAction world res) initial
  a `shouldBe`
    (Right $ Just $
     (CrawlTarget False
      Nothing [("Referer", "http://localhost:8000/simple_page.html?id=4")] False
      "http://localhost:8000/simple_page.html?id=5"))
  s `shouldBe` (IntMap.fromList [(1, def &
                                     originalArchive .~ world IntMap.! 1 &
                                     newPages .~ ["5"] &
                                     fragments .~ [V.empty])])

simpleDone :: IO ()
simpleDone = do
  world <- getWorld
  let res = (1, None V.empty)
      initial = testState (world IntMap.! 1) ["5", "6"]
      (a, s) = runState (parseAction world res) initial
  a `shouldBe` Right Nothing
  s `shouldBe` (IntMap.adjust (fragments .~ [V.empty]) 1 initial)

simpleFaulty :: IO ()
simpleFaulty = do
  world <- getWorld
  let res = (1, Faulty)
      initial = testState (world IntMap.! 1) ["5"]
      (a, s) = runState (parseAction world res) initial
  a `shouldBe` Left EndFaulty
  s `shouldBe` (IntMap.fromList [(1, def &
                                     originalArchive .~ world IntMap.! 1 &
                                     newestPagePurge .~ True)])

simpleLoop :: IO ()
simpleLoop = do
  world <- getWorld
  let res = (1, Result "2" V.empty)
      initial = testState (world IntMap.! 1) []
      (a, s) = runState (parseAction world res) initial
  a `shouldBe` Left (EndLoop 2)
  s `shouldBe` (IntMap.adjust (fragments .~ [V.empty]) 1 initial)

loopInResults :: IO ()
loopInResults = do
  world <- getWorld
  let res = (1, Result "5" V.empty)
      initial = testState (world IntMap.! 1) ["5", "6"]
      (a, s) = runState (parseAction world res) initial
  a `shouldBe` Left (EndLoop 0)
  s `shouldBe` (IntMap.adjust (fragments .~ [V.empty]) 1 initial)

loopInArchiveWithNewPages :: IO ()
loopInArchiveWithNewPages = do
  world <- getWorld
  let initial = testState (world IntMap.! 1) ["6", "5"]
      a = evalState (parseAction world (1, Result "2" V.empty)) initial
      b = evalState (parseAction world (1, Result "5" V.empty)) initial
  a `shouldBe` Left (EndLoop 4)
  b `shouldBe` Left (EndLoop 1)

overlapTest :: IO ()
overlapTest = do
  world <- getWorld
  let res = (1, Multi $ V.fromList ["2", "3", "4", "5", "6"])
      initial = testState (world IntMap.! 1) []
      (a, s) = runState (parseAction world res) initial
  a `shouldBe`
    (Right $ Just $
     (CrawlTarget False
      Nothing [("Referer", "http://localhost:8000/simple_page.html?id=5")] False
      "http://localhost:8000/simple_page.html?id=6"))
  s `shouldBe` (IntMap.fromList [(1, def &
                                     originalArchive .~ world IntMap.! 1 &
                                     newPages .~ ["6", "5"] &
                                     fragments .~ [V.empty, V.empty])])

staticURLTest :: IO ()
staticURLTest = do
  cfg <- fromJust . decode <$> B.readFile "test/files/rss_config.json"
  let pages = V.fromList ["another_page", "page2", "simple_page"]
      world = IntMap.fromList [(1, ArchiveEntry 3 pages 0 cfg)]
      res = (1, Multi $ V.fromList ["hello", "asdf"])
      initial = testState (world IntMap.! 1) []
      (a, s) = runState (parseAction world res) initial
  a `shouldBe` Right Nothing
  s `shouldBe` (IntMap.fromList [(1, def &
                                     originalArchive .~ world IntMap.! 1 &
                                     newPages .~ ["asdf", "hello"] &
                                     fragments .~ [V.empty, V.empty])])
