{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE OverloadedStrings #-}

module Crawler.Test.Command where

import Control.Concurrent
import Control.Exception
import Control.Lens
import Control.Monad.Trans.Except
import Data.Aeson (toJSON)
import Data.Default.Class
import qualified Data.IntMap as IntMap
import Data.IORef
import Data.Time.Clock
import qualified Data.Vector as V
import Test.Hspec

import Crawler.Command
import Crawler.Command.Types
import Crawler.DB
import Crawler.DB.Fetch
import Crawler.Types
import Crawler.Test.DB.Init

instance Show CrawlResult where
  show x = show $ toJSON x

commandTests :: Spec
commandTests = before_ initDB $ do
  it "can use autodiscover" testAutoDiscover
  it "can use crop after misleading pages" testMisleadCrop

withCrawlerCommand :: (Chan CrawlControl -> Chan CrawlResult -> CrawlComm -> IO ()) -> IO ()
withCrawlerCommand f = do
  settings <- getSettings
  let programCfg = dbConfig .~ settings $ def
  now <- getCurrentTime
  cmdCh<- newChan
  eventCh <- newChan
  ref <- newIORef $ Crawl now False [] def Nothing 0 mempty
  let conn = CrawlComm
        { sendData = (writeChan eventCh) :: CrawlResult -> IO ()
        , receiveData = readChan cmdCh
        , getCrawl = readIORef ref
        , adjust = atomicModifyIORef ref . ((,()) .)
        }
  bracket (forkIO $ crawlerApp programCfg conn) killThread (const (f cmdCh eventCh conn))

testAutoDiscover :: IO ()
testAutoDiscover = withCrawlerCommand $ \cmdCh eventCh _ -> do
  writeChan cmdCh $ CmdDiscover "http://localhost:8000/increasing?id=" "" "1" "2"
  x <- readChan eventCh
  x `shouldBe` CrawlDiscover [1]

testMisleadCrop :: IO ()
testMisleadCrop = withCrawlerCommand $ \cmdCh eventCh comm -> do
  settings <- getSettings
  let isEnd (CrawlDone _) = True
      isEnd _ = False
      feedParams f = f "http://localhost:8000/misleading/" ".html" Nothing Nothing
      whileM p f xs = do
        x <- f
        if p x then whileM p f $ x:xs else return $ x:xs
  writeChan cmdCh $ CmdAddPages ["page1"]
  readChan eventCh >>= shouldBe (CrawlAddPage 0 (Just "page1"))
  writeChan cmdCh $ CmdStart 6 feedParams
  xs <- whileM (not . isEnd) (readChan eventCh) []
  case head xs of
    CrawlDone ss -> do
      ss ^. newPages `shouldBe` ["deadend2", "deadend1", "page2"]
      crawl <- getCrawl comm
      crawl ^. initialArchive . archivePages `shouldBe` V.fromList ["page1"]
    _ -> expectationFailure "Last event was not a CrawlDone"
  writeChan cmdCh $ CmdCrop 2
  readChan eventCh >>= shouldBe (CrawlCrop 2)
  writeChan cmdCh $ CmdStart 3 feedParams
  xs' <- whileM (not . isEnd) (readChan eventCh) []
  case head xs' of
    CrawlDone ss -> do
      ss ^. newPages `shouldBe` ["page5", "page4", "page3"]
      crawl <- getCrawl comm
      crawl ^. initialArchive . archivePages `shouldBe` V.fromList ["page2", "page1"]
    _ -> expectationFailure "Last event was not a CrawlDone"
  writeChan cmdCh $ CmdSave 100 [] "" ""
  readChan eventCh >>= shouldBe (CrawlAck "success")
  (runExceptT $ withConn settings $ fetchPages' [100]) >>= shouldBe
    (Right $ IntMap.fromList
     [ (100, V.fromList $ map Just ["page5", "page4", "page3", "page2", "page1"] ) ]
    )
