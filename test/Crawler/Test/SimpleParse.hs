{-# LANGUAGE OverloadedStrings #-}

module Crawler.Test.SimpleParse (perlProcessTests) where

import Data.Aeson
import qualified Data.ByteString.Lazy as B
import Data.Default.Class
import Data.Maybe
import qualified Data.Vector as V
import System.Process (StdStream(NoStream))
import Test.Hspec

import Crawler.Parser.Perl.Comm
import Crawler.Types

perlProcessTests :: Spec
perlProcessTests = parallel $ do
  it "can process a simple file" simpleParse
  it "can parse two sites with one process" dualParse
  it "can use text hook with utf-8 content" utf8Parse
  it "survives bad response" badResponse

simpleParse :: IO ()
simpleParse = do
  cfg' <- decode <$> B.readFile "test/files/simple_config.json"
  cfg' `shouldSatisfy` isJust
  let cfg = fromJust cfg'
  parser <- startParser def
  inst <- startInstance parser cfg
  header <- B.readFile "test/files/simple_header"
  html <- B.readFile "test/files/simple_page.html"
  res <- feedPage inst "1" (header <> "\n" <> html)
  closeInstance inst
  closeParser parser
  res `shouldBe` Right (Result "2" V.empty) 

dualParse :: IO ()
dualParse = do
  cfg' <- decode <$> B.readFile "test/files/simple_config.json"
  cfg' `shouldSatisfy` isJust
  cfg2' <- decode <$> B.readFile "test/files/img_next_config.json"
  cfg2' `shouldSatisfy` isJust
  let cfg = fromJust cfg'
      cfg2 = fromJust cfg2'
  parser <- startParser def
  inst1 <- startInstance parser cfg
  inst2 <- startInstance parser cfg2
  header <- B.readFile "test/files/simple_header"
  html1 <- B.readFile "test/files/simple_page.html"
  html2 <- B.readFile "test/files/img_next1.html"
  res1 <- feedPage inst1 "1" (header <> "\n" <> html1)
  res2 <- feedPage inst2 "1" (header <> "\n" <> html2)
  closeInstance inst1
  closeInstance inst2
  closeParser parser
  res1 `shouldBe` Right (Result "2" V.empty)
  res2 `shouldBe` Right (Result "2" V.empty)

utf8Parse :: IO ()
utf8Parse = do
  cfg <- fromJust . decode <$> B.readFile "test/files/utf-8_config.json"
  parser <- startParser def
  inst <- startInstance parser cfg
  header <- B.readFile "test/files/simple_header"
  html <- B.readFile "test/files/utf-8_page.html"
  res <- feedPage inst "1" (header <> "\n" <> html)
  closeInstance inst
  closeParser parser
  res `shouldBe` Right (Result "2" V.empty)

badResponse :: IO ()
badResponse = do
  cfg <- fromJust . decode <$> B.readFile "test/files/simple_config.json"
  parser <- startParser' def NoStream
  inst <- startInstance parser cfg
  res <- feedPage inst "1" "garbage in garbage out"
  res `shouldBe` Right Faulty
  header <- B.readFile "test/files/simple_header"
  html <- B.readFile "test/files/simple_page.html"
  res2 <- feedPage inst "1" (header <> "\n" <> html)
  closeInstance inst
  closeParser parser
  res2 `shouldBe` Right (Result "2" V.empty)
