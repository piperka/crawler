{-# LANGUAGE OverloadedStrings #-}

module Crawler.Test.DB.Init where

import Control.Monad (void)
import Control.Monad.IO.Class
import qualified Data.ByteString.Char8
import System.Posix.User
import System.Process (system)

import Crawler.DB

initDB :: IO ()
initDB = void $ do
  name <- getEffectiveUserName
  system $ "psql crawler-test -U " <> name <>
    " -q -f test/files/db_init.sql -o /dev/null"

getSettings :: MonadIO m => m DBConfig
getSettings = do
  name <- Data.ByteString.Char8.pack <$> liftIO getEffectiveUserName
  return $ DBConfig ("postgresql://" <> name <> "@/crawler-test") "piperka"
