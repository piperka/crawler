{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Crawler.Test.Compensations where

import Control.Lens
import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Aeson
import qualified Data.ByteString.Lazy as B
import Data.Default.Class
import Data.Either
import qualified Data.IntMap as IntMap
import Data.Maybe (isJust, fromJust)
import qualified Data.Text as T
import qualified Data.Vector as V
import Test.Hspec

import Crawler.Compensations
import Crawler.Crawl.Logging
import Crawler.DB
import Crawler.DB.Fetch
import Crawler.DB.Log
import Crawler.DB.Store
import Crawler.Test.DB.Init
import Crawler.Types

compensationsTests :: Spec
compensationsTests = before_ initDB $ do
  it "can find compensations for loops" loopCompensationsTest
  it "can remove loop and adjust bookmarks" loopFixPerformTest

loopCompensationsTest :: IO ()
loopCompensationsTest = do
  cfg <- decode <$> B.readFile "test/files/simple_config.json"
  cfg `shouldSatisfy` isJust
  let cfg' = (fromJust cfg) {urlBase = "http://localhost:8000/increasing?id="}
      mkEntry :: [Int] -> ArchiveEntry
      mkEntry xs = ArchiveEntry (length xs - 1) (V.fromList $ map (T.pack . show) xs) 0 cfg'
      archive = IntMap.fromList
        [ (1, mkEntry [5, 2, 6, 3, 7, 4, 1])
        , (2, mkEntry [5, 4, 3, 2, 6, 7])
        , (3, cConfig .~ cfg' {startHook = ""} $ mkEntry [4, 3, 2, 5, 1])
        ]
      endEvents = IntMap.fromList
        [ (1, (7, TerminalEvent (EndLoop 2) 0 Nothing))
        , (2, (6, TerminalEvent (EndLoop 4) 0 Nothing))
        , (3, (5, TerminalEvent (EndLoop 3) 0 Nothing))
        ]
      target = IntMap.fromList
        [ (1, ReversedLoop 4 2)
        , (2, NoMatchForNext "8")
        , (3, NoNextLoop)
        ]
  res <- IntMap.fromList <$>
    (mapM (\(cid, ev) -> do
              let f = fromJust $ uncurry compensation ev
              (cid,) <$> f (IntMap.singleton cid $ archive IntMap.! cid)
          ) $ IntMap.assocs endEvents)
  res `shouldBe` target

loopFixPerformTest :: IO ()
loopFixPerformTest = do
  settings <- getSettings
  let programCfg = dbConfig .~ settings $ def
  res <- runExceptT $ do
    aState <- withConn settings $ fetchSingleArchive False 10
    lift $ aState `shouldSatisfy` isJust
    res1 <- lift $ crawlLog programCfg (fromJust aState) 50 False
    lift $ res1 `shouldSatisfy` isRight
    errs <- withConnTransactionally settings $ do
      saveCrawl $ either undefined id res1
      getLogEvents
    -- Compensate by removing the looped page
    lift $ errs `shouldSatisfy` isRight
    let comp = uncurry compensation $
               either undefined ((\(_,a,b) -> (fromIntegral a,b)) . head) errs
    comp' <- liftIO $ (fromJust comp) $ fromJust aState
    lift $ comp' `shouldBe` (RemoveLoop 1 2)
    withConnTransactionally settings $
      compensate 10 comp'
    aState2 <- withConn settings $ fetchSingleArchive False 10
    lift $ (view archivePages . head . IntMap.elems <$> aState2) `shouldBe`
      (Just $ V.fromList ["4", "3", "2", "1"])
    -- Crawl again
    res2 <- lift $ crawlLog programCfg (fromJust aState2) 50 False
    lift $ res2 `shouldSatisfy` isRight
    withConnTransactionally settings $ saveCrawl $ either undefined id res2
    -- Check DB contents after final save
    (archive2, subs) <- withConn settings $ (,)
      <$> fetchPages [10]
      <*> fetchBookmarks 10
    lift $ archive2 `shouldBe` IntMap.fromList
      [ (10, (6, V.fromList ["7", "6", "5", "4", "3", "2", "1"])) ]
    lift $ subs `shouldBe`
      [ (1, 0, 1)
      , (2, 1, 0)
      , (3, 1, 0)
      , (4, 1, 0)
      , (5, 1, 0)
      , (6, 1, 0)
      , (7, 1, 1)
      , (8, 2, 1)
      ]
    return ()
  res `shouldBe` Right ()
