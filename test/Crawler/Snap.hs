{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}

module Crawler.Snap (testServe) where

import Control.Applicative
import Control.Concurrent (forkIO, killThread)
import Control.Exception (bracket)
import Control.Lens
import Control.Monad.State
import Data.Aeson
import Data.Bool (bool)
import Data.ByteString (ByteString)
import qualified Data.ByteString as B
import Data.ByteString.Char8 (readInt, unpack)
import Data.IORef
import qualified Data.Map as M
import qualified Data.HashMap.Strict as H
import Data.Map.Syntax
import Data.Maybe
import qualified Data.Text as T
import Data.Word8
import Heist
import Heist.Compiled
import qualified Network.HTTP.Types.URI as URI
import Snap
import Snap.Snaplet.Heist
import Snap.Util.FileServe
import Snap.Util.GZip

data App = App
  { _heist :: Snaplet (Heist App)
  , _counter :: IORef Int
  }

makeLenses ''App

instance HasHeist App where
  heistLens = subSnaplet heist

testSplices :: Splices (Splice (Handler App App))
testSplices = do
  "plusone" ## return $ yieldRuntimeText $ do
    params <- lift getParams
    let p = fmap ((>) . fst) . readInt =<< listToMaybe =<< M.lookup "max" params
        n = maybe 1 fst $ readInt =<< listToMaybe =<< M.lookup "id" params
    return $ T.pack $ show $ maybe succ (>>= bool id succ) p $ n
  "otherParams" ## return $ yieldRuntimeText $ do
    params <- lift getParams
    -- Simple with no escaping
    return . T.pack . unpack .
      foldr (\a b -> a <> "&" <> b) "" .
      map (\(a,b) -> a <> "=" <> b) .
      filter ((/= "id") . fst) . map (fmap head) $ M.toList params
  "faultTest" ## deferMany (const runChildren) $
    fmap fst . (readInt =<<) <$> (lift $ getParam "fault") >>=
    maybe (return Nothing)
    (\n -> do
        c <- lift $ withTop' id $ view counter
        i <- liftIO $ modifyIORef c succ >> readIORef c
        if mod i n /= 0 then return Nothing else lift $ do
          modifyResponse $ setResponseStatus 500 "Test error"
          r <- getResponse
          writeBS "500 testing"
          finishWith r
    )

app :: SnapletInit App App
app = makeSnaplet "crawler-test" "Crawler test server." Nothing $ do
  h <- nestSnaplet "" heist $ heistInit' "templates" $ emptyHeistConfig
    & hcNamespace .~ "h"
    & hcCompiledSplices .~ testSplices
  addRoutes
    [ ("compressed_page.html", withCompression $ serveFile "test/files/simple_page.html")
    , ("loop.html", redirect "http://localhost:8000/loop.html")
    , ("fragments/redirecting_page.html", redirect "http://localhost:8000/fragments/page2.html")
    , ("consent.html", consentPage)
    , ("consent", consentDir "http://localhost:8000/consent.html")
    , ("consent_json", consentDir "http://localhost:8000/consent.html?json=1")
    , ("", serveDirectory "test/files")
    , ("redirecting", redirectingDir)
    ]
  wrapSite (<|> cHeistServe)
  wrapSite (redirectingQueryParam <|>)
  c <- liftIO $ newIORef 0
  return $ App h c

redirectingQueryParam :: Handler App App ()
redirectingQueryParam = do
  rq <- getRequest
  if isJust $ rqQueryParam "alwaysRedirect" rq
    then do
    let rd = URI.renderQuery True . filter ((/= "alwaysRedirect") . fst) .
             URI.parseQuery $ rqQueryString rq
    redirect $ rqContextPath rq <> rqPathInfo rq <> rd
    else pass

consentPage :: Handler App App ()
consentPage = do
  rq <- getRequest
  let ct = getHeader "Content-Type" rq
      useJson = (== Just ["1"]) $ rqQueryParam "json" rq
  setConsent <- if useJson
    then case ct of
           Just "application/json" -> do
             content <- decode <$> readRequestBody 1000
             return $ case content of
               Just (Object o) -> (Just (Bool True) ==) $ H.lookup "set_consent" o
               _ -> False
           _ -> return False
    else (Just ["1"] ==) . M.lookup "set_consent" <$> getPostParams
  case setConsent of
    True -> do
      modifyResponse $ addResponseCookie
        (Cookie "consent" "1" Nothing Nothing Nothing False False)
      redirect "http://localhost:8000/consent/page1.html"
    False -> serveFile "test/files/consent.html"

consentDir :: ByteString -> Handler App App ()
consentDir redirTo = do
  haveConsent <- maybe False ((== "1") . cookieValue) <$> getCookie "consent"
  case haveConsent of
    True -> serveDirectory "test/files/consent"
    False -> redirect redirTo

redirectingDir :: Handler App App ()
redirectingDir = do
  rq <- getRequest
  let pth = reverse $ B.split _slash $ rqPathInfo rq
  if isJust $ readInt $ head pth
    then redirect ("http://localhost:8000/redirecting/" <> rqPathInfo rq <> "/page.html")
    else serveFile $ "test/files/redirecting/" <> (unpack $ rqPathInfo rq)

-- To try out testServe with a browser, use "testServe getChar" in
-- ghci.
--
-- To run a test using testServe directly, give it as a parameter to
-- testServe.
testServe :: forall a. IO a -> IO a
testServe = bracket
  (forkIO $ serveSnapletNoArgParsing (setPort 8000 $ setVerbose False $ mempty) app)
  killThread . const
